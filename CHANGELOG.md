# Change Log

Liste des changements notables du serveur photo.

## Version 1.0.0 - 2022-10-16 - Version initiale

Début de la numérotation des versions et du changelog.

Fonctionnalités initiales :
- Deux modèles disjoints pour galeries et dossiers
- Permission par galerie pour voir/ajouter des photos/masquer des
  photos/gérer les permissions et info
- Formulaires pour éditer/créer des galeries/dossier
- Formulaire pour téléverser des photos, conversion des photos en JPG
- Formulaire pour modifier les permissions de galeries par utilisateurs, groupes
  ou lien d'accès
- Vue pour télécharger une galerie au format ZIP
- Typecheck avec mypy et tsc, formatage avec black et isort, lint avec flake8
- Application `config` pour gérer des réglages dynamiques
