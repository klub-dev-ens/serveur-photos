from typing import Any, TypeAlias, cast

from django import forms
from django.contrib.auth.models import Group, User
from django.core.exceptions import ValidationError

from .models import Folder, Gallery, GalleryPassword


class FolderForm(forms.ModelForm):
    """Formulaire pour l'édition/la création de dossier"""

    class Meta:
        model = Folder
        fields = (
            "parent",
            "name",
            "slug",
        )
        labels = {
            "parent": "Dossier parent",
            "name": "Nom du dossier",
        }

    @staticmethod
    def sort_key(choice: tuple[Any, str]) -> str:
        """Sorts folders according to their name"""
        if isinstance(choice[0], str):
            return ""
        return choice[1]

    def filter(self, choice: tuple[Any, str]) -> bool:
        """Returns True for choices which are not children of self.instance"""
        if not isinstance(choice[0], str):
            runner = choice[0].instance
            while runner is not None:
                if runner.id == self.instance.id:
                    return False
                runner = runner.parent
        return True

    def __init__(self, instance=None, **kwargs) -> None:
        """Override init to set smart values for parent.choices"""
        super().__init__(instance=instance, **kwargs)
        parent = cast(Any, self.fields["parent"])
        iterator = sorted(parent.choices, key=self.sort_key)
        if self.instance:
            iterator = list(filter(self.filter, iterator))
        parent.choices = iterator


class GalleryForm(forms.ModelForm):
    """Formulaire pour l'édition/la création de galleries"""

    class Meta:
        model = Gallery
        fields = (
            "parent",
            "name",
            "slug",
        )


class GalleryPermissionForm(forms.ModelForm):
    """Formulaire pour les permissions de galleries"""

    class Meta:
        model = Gallery
        fields = (
            "public",
            "hidden",
        )


class GroupPermissionForm(forms.Form):
    group = forms.ModelChoiceField(Group.objects.all(), widget=forms.HiddenInput())

    can_view = forms.BooleanField(required=False)
    can_upload = forms.BooleanField(required=False)
    can_delete = forms.BooleanField(required=False)
    can_edit = forms.BooleanField(required=False)

    def group_name(self) -> Group | None:
        """Small hack to display real name (and not value)
        of the hidden field group"""
        group = self.data.get("group")
        if group is None:
            group = self.initial.get("group")
        return group

    def save_permissions(self, gallery: Gallery):
        """Adds relevant permissions to the given gallery"""
        group = self.cleaned_data["group"]
        if self.cleaned_data["can_view"]:
            gallery.can_view_groups.add(group)
        if self.cleaned_data["can_upload"]:
            gallery.can_upload_groups.add(group)
        if self.cleaned_data["can_delete"]:
            gallery.can_delete_groups.add(group)
        if self.cleaned_data["can_edit"]:
            gallery.can_edit_groups.add(group)


class UniqueGroupFormset(forms.BaseFormSet):
    """Formset that checks all forms are unique"""

    unique_field: str = "group"
    error_message: str = "Ce groupe apparaît plusieurs fois"

    def clean(self) -> None:
        super().clean()
        values = set()
        for form in self.forms:
            value = form.cleaned_data.get(self.unique_field)
            if value is None:
                continue
            if value in values:
                form.add_error(self.unique_field, self.error_message)
            values.add(value)


GroupFormsetType: TypeAlias = "forms.BaseFormSet[GroupPermissionForm]"


def group_permission_formset_factory() -> type[GroupFormsetType]:
    group = Group.objects.all().count()
    return forms.formset_factory(
        GroupPermissionForm,
        extra=0,
        min_num=group,
        max_num=group,
        can_order=False,
        can_delete=False,
        formset=UniqueGroupFormset,
    )


class UserPermissionForm(forms.Form):
    user = forms.CharField(
        max_length=100,
        widget=forms.TextInput(attrs={"placeholder": "Nom d'utilisateur"}),
    )

    can_view = forms.BooleanField(required=False)
    can_upload = forms.BooleanField(required=False)
    can_delete = forms.BooleanField(required=False)
    can_edit = forms.BooleanField(required=False)

    user_obj: User

    def clean(self) -> dict[str, Any]:
        """Checks that the text field user actually designates a user
        and sets user_obj"""
        username = self.cleaned_data.get("user")
        if username is None:
            return self.cleaned_data
        try:
            self.user_obj = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValidationError({"user": "Nom d'utilisateur inconnu"})
        return self.cleaned_data

    def save_permissions(self, gallery: Gallery) -> None:
        """Adds relevant permissions to the given gallery"""
        if not self.cleaned_data:
            # Dynamic formset can create empty forms...
            return
        if self.cleaned_data.get("can_view") is True:
            gallery.can_view_users.add(self.user_obj)
        if self.cleaned_data.get("can_upload") is True:
            gallery.can_upload_users.add(self.user_obj)
        if self.cleaned_data.get("can_delete") is True:
            gallery.can_delete_users.add(self.user_obj)
        if self.cleaned_data.get("can_edit") is True:
            gallery.can_edit_users.add(self.user_obj)


class UniqueUserFormset(UniqueGroupFormset):
    unique_field = "user"
    error_message = "Cet utilisateur apparait plusieurs fois"


UserFormsetType: TypeAlias = "forms.BaseFormSet[UserPermissionForm]"
UserPermissionFormset: type[UserFormsetType] = forms.formset_factory(
    UserPermissionForm,
    extra=2,
    can_order=False,
    can_delete=True,
    formset=UniqueUserFormset,
)


class PhotoUploadForm(forms.Form):
    filename = forms.CharField(required=False)
    file = forms.FileField(widget=forms.ClearableFileInput(attrs={"allow_multiple_selected": Trueallow_multiple_selected}))


class DateInput(forms.DateInput):
    input_type = "date"


class GalleryPasswordForm(forms.ModelForm):
    class Meta:
        model = GalleryPassword
        fields = ("expires",)
        labels = {
            "expires": "",
        }
        widgets = {
            "expires": forms.DateInput(format="%Y-%m-%d", attrs={"type": "date"}),
        }
        help_texts = {
            "expires": None,
        }

    def url(self) -> str:
        return str(self.instance)


GalleryPasswordFormsetType: TypeAlias = (
    "forms.BaseModelFormSet[GalleryPassword, GalleryPasswordForm]"
)


def gallery_password_formset_factory(
    gallery: Gallery,
) -> type[GalleryPasswordFormsetType]:
    """Create a gallery password formset that checks that
    all instances point to the gallery given as argument"""

    class CheckGalleryFormset(forms.BaseModelFormSet):
        """Custom formset class to add our verification"""

        def clean(self) -> None:
            super().clean()
            error_msg = (
                "La galerie d'une des clés ne correspond pas à la galerie actuelle"
            )
            for form in self.forms:
                try:
                    if form.instance.gallery != gallery:
                        raise forms.ValidationError(error_msg)
                except Gallery.DoesNotExist:
                    raise forms.ValidationError(error_msg)

    passwords = gallery.gallerypassword_set.all().count()

    return forms.modelformset_factory(
        GalleryPassword,
        form=GalleryPasswordForm,
        extra=0,
        can_order=False,
        can_delete=False,
        max_num=passwords,
        min_num=passwords,
        formset=CheckGalleryFormset,
    )
