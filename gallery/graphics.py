"""
Graphic operation, use the create_photo(<FileObject>, Gallery)
to create a new photo
"""

from datetime import datetime
from hashlib import sha256
from os import stat
from typing import TypeVar, cast

from django.conf import settings
from django.core.files.uploadedfile import UploadedFile
from django.utils.timezone import make_aware, now
from PIL import UnidentifiedImageError
from PIL.Image import Image, open as pil_open

from config.constants import (
    IMAGE_DEFAULT_FILENAME,
    JPEG_IMAGE_QUALITY,
    JPEG_THUMBNAIL_QUALITY,
)

from .models import Gallery, Photo
from .utils import ensure_unique

# from zipfile import BadZipFile, LargeZipFile, ZipFile


T = TypeVar("T")
U = TypeVar("U")


def getattr_or(obj, attr: str, default: U) -> T | U:
    if hasattr(obj, attr):
        return cast(T | U, getattr(obj, attr))
    return default


def image_date(image: Image) -> datetime | None:
    "returns the image date from image (if available)"
    tags = [
        36867,  # DateTimeOriginal
        36868,  # DateTimeDigitized
        306,  # DateTime
    ]
    exif = image.getexif()
    dat = None
    for t in tags:
        dat = exif.get(t)
        # PIL.PILLOW_VERSION >= 3.0 returns a tuple
        dat = dat[0] if isinstance(dat, tuple) else dat
        if dat is not None:
            break
    if dat is None:
        return None
    try:
        return datetime.strptime(dat, "%Y:%m:%d %H:%M:%S")
    except ValueError:
        return None


def create_photo(
    file: UploadedFile, gallery: Gallery, filename: str | None
) -> Photo | None:
    """Write the file in the correct photo position"""
    dimensions = settings.THUMBNAIL_SIZE
    # Open image
    try:
        image = pil_open(file)
    except (UnidentifiedImageError, ValueError):
        return None
    # Get image data
    img_filename: str = getattr_or(image, "filename", "unnamed")
    width: int | None = getattr_or(image, "width", None)
    height: int | None = getattr_or(image, "height", None)
    date_created = image_date(image)
    if date_created is not None:
        date_created = make_aware(date_created)
    # Make new name
    if filename is None or filename == "":
        if img_filename != "":
            filename = img_filename
        else:
            filename = IMAGE_DEFAULT_FILENAME
    photos = [photo.filename for photo in Photo.objects.filter(gallery=gallery)]
    name, _ = ensure_unique(filename, photos)
    is_jpeg = image.format == "JPEG"
    # Create Photo object
    photo = Photo(
        filename=name + ".jpg",
        date_uploaded=now(),
        date_created=date_created,
        width=width,
        height=height,
        gallery=gallery,
    )
    # Ensure paths exist
    path_photo = settings.PHOTO_ROOT / photo.path()
    path_thumbs = settings.PHOTO_ROOT / photo.thumbnail_path()
    path_photo.parent.mkdir(parents=True, exist_ok=True)
    path_thumbs.parent.mkdir(parents=True, exist_ok=True)
    # Save image and thumbnail
    if image.mode not in ["L", "RGB", "CMYK"]:
        image = image.convert("RGB")
    try:
        if is_jpeg:
            # No need to convert, save as is
            # Using image.save would change compression...
            with open(str(path_photo), "wb") as target:
                for chunks in file.chunks():
                    target.write(chunks)
        else:
            image.save(str(path_photo), quality=JPEG_IMAGE_QUALITY)
    except Exception:
        return None
    image.thumbnail((dimensions.width, dimensions.height))
    try:
        image.save(str(path_thumbs), quality=JPEG_THUMBNAIL_QUALITY)
    except Exception:
        return None
    # Set size attributes
    photo.size = stat(path_photo).st_size
    photo.thumb_size = stat(path_thumbs).st_size
    # Set hash
    try:
        with open(path_photo, "rb") as file_handle:
            photo.sha256 = sha256(file_handle.read()).digest()
    except IOError:
        return None

    # Save photo object to database
    photo.save()
    return photo  # success


# def extract_photos_from_zip(file: UploadedFile, gallery:Gallery):
#     """Read all photos in a zip archive"""
#     try:
#         with ZipFile(file) as zipfile:
#             for name in zipfile.namelist():
#                 with zipfile.open(name, mode="rb") as photo:
#                     obj = create_photo(
#                         UploadedFile(photo.read(), name=name),
#                         gallery,
#                         name)
#     except BadZipFile or LargeZipFile or IOError:
#         return None
