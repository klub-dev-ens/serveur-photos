from datetime import date
from os import remove, rename, stat
from pathlib import Path
from typing import TypeVar
from zipfile import ZIP_DEFLATED, ZipFile

from django.conf import settings
from django.contrib.auth.models import AnonymousUser, Group, User
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse

from config.constants import FOLDER_PATH_SEPARATOR, KEY_LENGTH, KEY_QUERY_PARAM_NAME

from .utils import ensure_unique, pretty_size

AnyUser = User | AnonymousUser

T = TypeVar("T", bound=models.Model)


class PhotoQuerySet(models.QuerySet[T]):
    """Methods that appear both in the manager and queryset."""

    def delete(self) -> tuple[int, dict[str, int]]:
        """
        The default django admin bulk-delete is more efficient,
        But for photos, we need to call each individual photo's delete method to remove
        the associated files
        """
        count = 0
        for photo in self.all():
            count += 1
            photo.delete()
        return count, dict()


class Photo(models.Model):
    """An entry referencing a photo on disk"""

    objects = PhotoQuerySet.as_manager()  # type: ignore[var-annotated]

    filename = models.CharField(
        verbose_name="Nom du fichier",
        help_text="Chemin relatif à la gallerie",
        max_length=512,
    )

    date_created = models.DateTimeField(
        verbose_name="Date de création", null=True, blank=True
    )
    date_uploaded = models.DateTimeField(verbose_name="Date d'upload")

    gallery = models.ForeignKey(
        "Gallery", verbose_name="galerie", on_delete=models.CASCADE
    )
    height = models.IntegerField(verbose_name="hauteur", null=True, blank=True)
    width = models.IntegerField(verbose_name="largeur", null=True, blank=True)

    size = models.IntegerField(verbose_name="taille (en octets)", default=0)
    thumb_size = models.IntegerField(
        verbose_name="taille du thumbnail (en octets)", default=0
    )

    visible = models.BooleanField(
        verbose_name="visible",
        default=True,
        help_text="Décocher pour masquer la photo sans la supprimer pour autant",
    )
    sha256 = models.BinaryField(
        verbose_name="Hash SHA 256",
        help_text="Ce hash sert à identifier des potentiels duplicata",
        max_length=256,
    )

    def path(self) -> str:
        """Returns the path relative to PHOTO_ROOT"""
        return (
            settings.PHOTO_PATH.format(GALLERY_PATH=self.gallery.path) + self.filename
        )

    def thumbnail_path(self) -> str:
        """Returns path to the thumbnail, relative to PHOTO_ROOT"""
        return (
            settings.THUMBNAIL_PATH.format(GALLERY_PATH=self.gallery.path)
            + self.filename
        )

    def pretty_size(self) -> str:
        """File size in human readable format"""
        return pretty_size(self.size + self.thumb_size)

    pretty_size.__name__ = "Taille"

    def __str__(self) -> str:
        return f"{self.gallery}/{self.filename}"

    def remove_from_previews(self) -> None:
        """Removes a photo from any previews it may appear in"""
        for folder in self.folder_previews.all():
            folder.preview = None
            folder.save()
        if self.gallery.preview == self:
            self.gallery.preview = None
            self.gallery.save()

    def delete(self, **kwargs) -> tuple[int, dict[str, int]]:  # type: ignore[override]
        """Also delete associated files"""
        self.remove_from_previews()
        remove(settings.PHOTO_ROOT / self.path())
        remove(settings.PHOTO_ROOT / self.thumbnail_path())
        return super().delete(**kwargs)

    def save(self, **kwargs) -> None:  # type: ignore[override]
        """Move photos if their path has changed on save
        Also updates gallery ZIP status"""
        if not Photo.objects.filter(pk=self.pk).exists():
            self.gallery.zip_up_to_date = False
            self.gallery.save()
            return super().save(**kwargs)
        old = Photo.objects.get(pk=self.pk)
        if old.path() == self.path():
            if old.visible != self.visible:
                self.gallery.zip_up_to_date = False
                self.gallery.save()
            return super().save(**kwargs)
        self.gallery.zip_up_to_date = False
        self.gallery.save()
        # Remove from preview if moving accross galleries
        if self.gallery != old.gallery:
            old.remove_from_previews()
            old.gallery.zip_up_to_date = False
            old.gallery.save()
        # Ensure new path is unique
        photos = [
            photo.filename
            for photo in Photo.objects.filter(gallery=self.gallery).exclude(pk=self.pk)
        ]
        name, extension = ensure_unique(self.filename, photos)
        self.filename = name + extension
        # Move photo file
        rename(settings.PHOTO_ROOT / old.path(), settings.PHOTO_ROOT / self.path())
        rename(
            settings.PHOTO_ROOT / old.thumbnail_path(),
            settings.PHOTO_ROOT / self.thumbnail_path(),
        )
        return super().save(**kwargs)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["filename", "gallery"],
                name="unique_photo_path",
            )
        ]
        verbose_name = "photo"
        ordering = ("-date_uploaded",)


# ===========================================================
# Galeries
# ===========================================================


class Folder(models.Model):
    """Folders used to organize galleries
    Eg. /Évènement/
        /Évènement/2021-2022"""

    parent = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text="Laisser vide pour la racine",
        related_name="children",
    )

    name = models.CharField(
        verbose_name="nom",
        max_length=64,
    )
    slug = models.SlugField(
        verbose_name="URL d'accès",
        max_length=64,
        help_text="Ne contient que A-Z, a-z, 0-9, _, -",
    )
    preview = models.ForeignKey(
        Photo,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Miniature",
        related_name="folder_previews",
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["slug", "parent"],
                name="unique_folder_url",
            ),
            # The above doesn't check uniqueness when parent=None,
            # so we add a second constraint
            models.UniqueConstraint(
                fields=["slug"],
                condition=models.Q(parent=None),
                name="unique_root_folder_url",
            ),
        ]
        verbose_name = "dossier"

    @classmethod
    def get_subfolders(cls, folder: "Folder | None") -> "models.QuerySet[Folder]":
        """Returns the list of (direct) subfolders of folder
        use folder=None for root subfolders"""
        return cls.objects.filter(parent=folder).order_by("name", "slug")

    @staticmethod
    def get_galleries(folder: "Folder | None") -> "models.QuerySet[Gallery]":
        """Returns the list of (direct) gallery children of folder
        use folder=None for root galleries"""
        return Gallery.objects.filter(parent=folder).order_by("-date_created")

    def can_view(self, user: AnyUser, key: str | None) -> bool:
        """True if the user can view this folder
        (if can he view any of the contained galleries)"""
        return (
            any(gallery.can_view(user, key) for gallery in self.get_galleries(self))
            or any(folder.can_view(user, key) for folder in self.get_subfolders(self))
            or self.contains_no_galleries()
        )

    def contains_no_galleries(self) -> bool:
        """True if folder (and all subfolders)
        contains no gallery"""
        return (not Folder.get_galleries(self).exists()) and all(
            folder.contains_no_galleries() for folder in Folder.get_subfolders(self)
        )

    @classmethod
    def from_path(cls, path: list[str]) -> "Folder | None":
        """Get a folder from an input path
        Returns None for the root path
        Raises ValueError if folder does not exist"""
        folder = None
        for slug in path:
            try:
                folder = cls.objects.get(parent=folder, slug=slug)
            except Folder.DoesNotExist:
                raise ValueError(f"Folder does not exist: {path}")
        return folder

    def __str__(self) -> str:
        """The folder representation
        E.G. path/to/folder/"""
        return encode_path(self)

    def is_ancestor_of(self, obj: "Folder | Gallery | None") -> bool:
        """Returns true if self is a parent of obj
        (also true if self == obj)"""
        if obj is None:
            return False
        if isinstance(obj, Folder) and self.pk == obj.pk:
            return True
        return self.is_ancestor_of(obj.parent)

    def clean(self) -> None:
        """Checks for circular folder paths"""
        super().clean()
        runner = self.parent
        found_ids = set((self.pk,))
        while runner is not None:
            if runner.pk in found_ids:
                raise ValidationError(
                    {"parent": "Chemin circulaire: ce parent est un fils de ce dossier"}
                )
            found_ids.add(runner.pk)
            runner = runner.parent

    def dynamic_preview(self, user: AnyUser, key: str | None) -> Photo | None:
        """Dynamic folder preview
        - self.preview if defined and accessible
        - a gallery dynamic preview from an accessible sub-gallery otherwise
        None if no accessible sub-galleries"""
        if self.preview and self.preview.gallery.can_view(user, key):
            return self.preview
        for gallery in self.get_galleries(self):
            if gallery.can_view(user, key) and gallery.dynamic_preview():
                return gallery.dynamic_preview()
        for folder in self.get_subfolders(self):
            preview = folder.dynamic_preview(user, key)
            if preview and folder.can_view(user, key):
                return preview
        return None


class Gallery(models.Model):
    """A photo gallery (the folder actually containing photos)

    Corresponds to a unique folder under PHOTO_ROOT/gallery_slug-id/"""

    name = models.CharField(
        verbose_name="nom",
        max_length=64,
    )
    slug = models.SlugField(verbose_name="chemin d'URL", max_length=64)
    path = models.CharField(
        verbose_name="Dossier",
        max_length=256,
        help_text="Chemin sur le système de fichier (relatif à PHOTO_ROOT). "
        "Ne pas modifier (sinon il également renommer le dossier sur le serveur)",
        unique=True,
    )

    parent = models.ForeignKey(
        Folder,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Dossier parent",
    )

    preview = models.ForeignKey(
        Photo,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Miniature",
        related_name="+",
    )
    date_created = models.DateTimeField(verbose_name="date de création")

    public = models.BooleanField(
        verbose_name="publique",
        help_text="Galerie visible par tous, y compris les personnes non-connectées. "
        "Ignore toutes les autres permissions de lecture",
        default=False,
    )
    hidden = models.BooleanField(
        verbose_name="masquée",
        help_text='Galerie visible uniquement par les utilisateurs qui ont la permission "gérer".',
        default=False,
    )

    # Permissions (lecture, upload...)
    can_view_users = models.ManyToManyField(
        User,
        verbose_name="Utilisateurs pouvant voir cette galerie",
        related_name="+",
        blank=True,
    )
    can_view_groups = models.ManyToManyField(
        Group,
        verbose_name="Groupes pouvant voir cette galerie",
        related_name="+",
        blank=True,
    )

    can_upload_users = models.ManyToManyField(
        User,
        verbose_name="Utilisateurs pouvant ajouter des photos",
        related_name="+",
        blank=True,
    )
    can_upload_groups = models.ManyToManyField(
        Group,
        verbose_name="Groupes pouvant ajouter des photos",
        related_name="+",
        blank=True,
    )

    can_delete_users = models.ManyToManyField(
        User,
        verbose_name="Utilisateurs pouvant supprimer des photos",
        related_name="+",
        blank=True,
    )
    can_delete_groups = models.ManyToManyField(
        Group,
        verbose_name="Groupes pouvant supprimer des photos",
        related_name="+",
        blank=True,
    )

    can_edit_users = models.ManyToManyField(
        User,
        verbose_name="Utilisateurs pouvant gérer la galerie",
        related_name="+",
        blank=True,
    )
    can_edit_groups = models.ManyToManyField(
        Group,
        verbose_name="Groupes pouvant gérer la galerie",
        related_name="+",
        blank=True,
    )
    zip_up_to_date = models.BooleanField(
        verbose_name="Fichier ZIP à jour",
        help_text="Si faux, le fichier sera regénérer au prochain téléchargement",
        default=False,
    )
    zip_size = models.IntegerField(
        verbose_name="Taille du fichier ZIP (o)",
        default=0,
    )

    class Meta:
        unique_together = (("slug", "parent"),)
        constraints = [
            models.UniqueConstraint(
                fields=["slug", "parent"],
                name="unique_gallery_url",
            ),
            # The above doesn't check uniqueness when parent=None,
            # so we add a second constraint
            models.UniqueConstraint(
                fields=["slug"],
                condition=models.Q(parent=None),
                name="unique_root_gallery_url",
            ),
        ]

        verbose_name = "galerie"

    def true_path(self) -> Path:
        """The actual path on disk of this gallery"""
        return settings.PHOTO_ROOT / self.path

    def save(self, **kwargs) -> None:  # type: ignore[override]
        """Move gallery folder if its path has changed on save"""
        if not Gallery.objects.filter(pk=self.pk).exists():
            return super().save(**kwargs)
        old = Gallery.objects.get(pk=self.pk)
        if old.path == self.path:
            return super().save(**kwargs)
        self.path = self.make_path_unique(self.path)
        rename(old.true_path(), self.true_path())
        return super().save(**kwargs)

    def clean(self) -> None:
        """Checks for path conflicts"""
        super().clean()
        if self.preview is not None and self.preview.gallery.pk != self.pk:
            raise ValidationError(
                {"preview": "Cette photo n'appartient pas à cette galerie"}
            )

    def can_view(self, user: AnyUser, password: str | None) -> bool:
        """True if user has read permission to this gallery"""
        if self.hidden:
            return self.can_edit(user)
        if self.public:
            return True
        if password is not None:
            try:
                key = self.gallerypassword_set.get(key=password)
            except GalleryPassword.DoesNotExist:
                pass
            else:
                if key.expires is None or key.expires >= date.today():
                    return True
        if user.is_authenticated:
            return (
                user.is_superuser
                or user in self.can_view_users.all()
                or self.can_view_groups.filter(
                    id__in=(group.pk for group in user.groups.all())
                ).exists()
            )
        return False

    def can_upload(self, user: AnyUser) -> bool:
        """True if user has upload permission to this gallery"""
        if user.is_authenticated:
            return (
                user.is_superuser
                or user in self.can_upload_users.all()
                or self.can_upload_groups.filter(
                    id__in=(group.pk for group in user.groups.all())
                ).exists()
            )
        return False

    def can_delete(self, user: AnyUser) -> bool:
        """True if user has delete photo permission to this gallery"""
        if user.is_authenticated:
            return (
                user.is_superuser
                or user in self.can_delete_users.all()
                or self.can_delete_groups.filter(
                    id__in=(group.pk for group in user.groups.all())
                ).exists()
            )
        return False

    def can_edit(self, user: AnyUser) -> bool:
        """True if user has edit permission to this gallery"""
        if user.is_authenticated:
            return (
                user.is_superuser
                or user in self.can_edit_users.all()
                or self.can_edit_groups.filter(
                    id__in=(group.pk for group in user.groups.all())
                ).exists()
            )
        return False

    @classmethod
    def from_path(cls, path: list[str]) -> "Gallery":
        """Return the gallery designated by the given path
        raises ValueError if invalid"""
        if path == []:
            raise ValueError("Gallery from empty path")
        gallery_slug = path.pop()
        folder = Folder.from_path(path)
        try:
            return Gallery.objects.get(parent=folder, slug=gallery_slug)
        except Gallery.DoesNotExist:
            raise ValueError(f"Gallery does not exist: '{path}'")

    def __str__(self) -> str:
        """Gallery representation: path/to/gallery"""
        return encode_path(self)

    def make_path_unique(self, base: str) -> str:
        all = (g.path for g in Gallery.objects.exclude(pk=self.pk))
        path, extension = ensure_unique(base, all, new_suffix="")
        return path + extension

    def make_path(self) -> None:
        """Initialize self.path to a new unique value of the form
        YYYY-MM-DD-NAME[-UNIQUE_NUMBER]"""
        base = self.date_created.strftime("%Y-%m-%d") + "-" + self.slug
        self.path = self.make_path_unique(base)

    def size(self) -> int:
        """Sum of sizes of contained files"""
        sums = self.photo_set.aggregate(
            sz=models.Sum("size"),
            t_sz=models.Sum("thumb_size"),
        )
        size: int = 0 if sums["sz"] is None else sums["sz"]
        thumb_size: int = 0 if sums["t_sz"] is None else sums["t_sz"]
        return size + thumb_size

    def pretty_size(self) -> str:
        return pretty_size(self.size())

    pretty_size.__name__ = "Taille"

    def dynamic_preview(self) -> Photo | None:
        """Renvoie une vignette dynamique:
        - celle définie si existante
        - une photo de la galerie sinon
        - None si la galerie est vide"""
        if self.preview:
            return self.preview
        return self.photo_set.filter(visible=True).first()

    def clear_permissions(self) -> None:
        """Clears all permissions for users and groups"""
        self.can_view_groups.clear()
        self.can_view_users.clear()
        self.can_upload_groups.clear()
        self.can_upload_users.clear()
        self.can_delete_groups.clear()
        self.can_delete_users.clear()
        self.can_edit_groups.clear()
        self.can_edit_users.clear()

    def zip_path(self) -> str:
        """Chemin relatif a PHOTO_ROOT du fichier ZIP"""
        return settings.ZIP_PATH.format(GALLERY_PATH=self.path, GALLERY_SLUG=self.slug)

    def make_zip(self) -> None:
        """Create a ZIP archive with all our visible photos
        This can take a while..."""
        if self.zip_up_to_date:
            return
        zip_path = settings.PHOTO_ROOT / self.zip_path()
        with ZipFile(zip_path, "w", ZIP_DEFLATED) as zip:
            for photo in self.photo_set.filter(visible=True):
                zip.write(settings.PHOTO_ROOT / photo.path(), arcname=photo.filename)
        self.zip_up_to_date = True
        self.zip_size = stat(zip_path).st_size
        self.save()

    def zip_pretty_size(self) -> str:
        """Pretty print zip file size"""
        return pretty_size(self.zip_size)

    def set_date_from_photos(self) -> None:
        """Set self.date to the earliest photo date (file or taken)
        Usefull when importing a pre-existing gallery"""
        date = self.date_created
        for photo in self.photo_set.all():
            if photo.date_created is not None and photo.date_created < date:
                date = photo.date_created
            if photo.date_uploaded < date:
                date = photo.date_uploaded
        if self.date_created != date:
            self.date_created = date
            self.save()


class GalleryPassword(models.Model):
    """mot de passe pour accès non-authentifié"""

    key = models.SlugField("clé", unique=True, max_length=KEY_LENGTH)
    expires = models.DateField(
        verbose_name="date d'expiration",
        null=True,
        blank=True,
        help_text="Laisser vide pour aucune expiration",
    )
    gallery = models.ForeignKey(
        Gallery, verbose_name="galerie", on_delete=models.CASCADE
    )

    def __str__(self) -> str:
        return (
            reverse("gallery", kwargs={"path": str(self.gallery)})
            + f"?{KEY_QUERY_PARAM_NAME}={self.key}"
        )

    def url(self, domain: str) -> str:
        return "https://" + domain + str(self)

    class Meta:
        verbose_name = "clé d'accès galerie"
        verbose_name_plural = "clés d'accès galerie"


# ================================================================
# Path utility functions
# ================================================================


def encode_path(obj: Gallery | Folder | None) -> str:
    """Returns the corresponding path (to use in urls)"""
    if obj is None:
        return FOLDER_PATH_SEPARATOR
    suffix = FOLDER_PATH_SEPARATOR if isinstance(obj, Folder) else ""
    return encode_path(obj.parent) + obj.slug + suffix
