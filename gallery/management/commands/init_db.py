from django.core.management.base import BaseCommand

from ...utils import (
    get_bds_group,
    get_clipper_group,
    get_cof_group,
    get_permission,
    get_tous_group,
)
from ...views.mixins import (
    CHANGE_FOLDER_PERMISSION,
    CREATE_FOLDER_PERMISSION,
    CREATE_GALLERY_PERMISSION,
    DELETE_FOLDER_PERMISSION,
    DELETE_GALLERY_PERMISSION,
)


class Command(BaseCommand):
    """Initialise the DB by giving COF and BDS groups
    their permission"""

    # All 4 permissions for these models
    permissions = [
        "gallery.view_gallery",
        CREATE_GALLERY_PERMISSION,
        DELETE_GALLERY_PERMISSION,
        "gallery.change_gallery",
        "gallery.view_folder",
        DELETE_FOLDER_PERMISSION,
        CHANGE_FOLDER_PERMISSION,
        CREATE_FOLDER_PERMISSION,
        "gallery.view_photo",
        "gallery.add_photo",
        "gallery.delete_photo",
        "gallery.change_photo",
        "gallery.view_gallerypassword",
        "gallery.add_gallerypassword",
        "gallery.delete_gallerypassword",
        "gallery.change_gallerypassword",
        "auth.view_user",
        "auth.add_user",
        "auth.delete_user",
        "auth.change_user",
        "auth.view_group",
        "auth.add_group",
        "auth.delete_group",
        "auth.change_group",
        "config.view_siteconfig",
        "config.add_siteconfig",
        "config.delete_siteconfig",
        "config.change_siteconfig",
    ]

    def handle(self, *args, **kwargs) -> None:
        """Gives default permission to cof and BDS group"""
        # Create groups clipper and tous
        get_clipper_group()
        get_tous_group()
        special_groups = (get_bds_group(), get_cof_group())
        # Create COF and BDS groups with permissions
        for perm in self.permissions:
            p = get_permission(perm)
            for groups in special_groups:
                groups.permissions.add(p)
