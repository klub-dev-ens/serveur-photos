from datetime import timedelta
from pathlib import Path
from warnings import catch_warnings

from django.conf import settings
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.utils.timezone import now

from ...models import Gallery, GalleryPassword, Photo
from ...tests.utils import (
    create_folder,
    create_gallery,
    create_user,
    import_from_folder,
)
from ...utils import (
    get_bds_group,
    get_clipper_group,
    get_cof_group,
    get_group_by_name,
    get_tous_group,
)
from .init_db import Command as init_db

PASSWORD = "0000"


class Command(BaseCommand):
    """
    Command to quickly populate an example DB
    """

    verbose: bool = True

    def log(self, message: str):
        """Displays a message if verbose is set"""
        if self.verbose:
            self.stdout.write(message)

    def create_users(self) -> None:
        """
        Créer des utilisateurs pour la DB de test.
        Leur mots de passe sont '0000'
        - admin (superuser)
        - clipper, cof, bds (dans les groupes de même nom)
        - exte
        """
        self.log(
            "Creating users: 'admin', 'clipper', 'cof', 'bds', 'exte' with password '0000'"
        )
        clipper = get_clipper_group().pk
        cof = get_cof_group().pk
        bds = get_bds_group().pk

        user = create_user("admin", "admin@mail.fr", PASSWORD, superuser=True)
        user.save()
        user = create_user("clipper", "clipper@mail.fr", PASSWORD, groups=[clipper])
        user.save()
        user = create_user("cof", "cof@mail.fr", PASSWORD, groups=[clipper, cof])
        user.is_staff = True
        user.save()
        user = create_user("bds", "bds@mail.fr", PASSWORD, groups=[clipper, bds])
        user.is_staff = True
        user.save()
        user = create_user("exte", "exte@mail.fr", PASSWORD)
        user.save()

        init_db().handle()

    def create_groups(self) -> None:
        self.log("Creating groups: 'tous', 'clipper', 'cof', 'bds' and two others")
        get_clipper_group()
        get_cof_group()
        get_bds_group()
        get_tous_group()
        get_group_by_name("groupe4")
        get_group_by_name("Groupe 5")

    def mk_folder(self, parent: str | None, name: str, slug: str) -> None:
        """Create a new folder"""
        folder = create_folder(parent, name, slug)
        self.log(" - {}".format(folder))

    def create_folders(self) -> None:
        """Create a bunch of folders to store galleries"""
        self.log("Creating folders:")
        self.mk_folder(None, "COF", "cof")
        self.mk_folder(None, "BDS", "bds")
        self.mk_folder(None, "Vide", "empty")
        self.mk_folder("COF", "Évènements", "evenements")
        self.mk_folder("COF", "Soirées", "soiree")
        self.mk_folder("COF", "K-Fêt", "k-fet")

    def mk_gallery(
        self, parent: str | None, name: str, slug: str, photos: Path | None
    ) -> Gallery:
        gallery = create_gallery(parent, name, slug)
        gallery.save()
        added: list[Photo] = []
        failed: list[str] = []
        if photos is not None:
            added, failed = import_from_folder(gallery, photos)
            self.log(" - {} with {} photos".format(gallery, len(added)))
            for err in failed:
                self.log("   ERROR loading photo: {}".format(photos / err))
        else:
            self.log(" - {} (no photos)".format(gallery))
        gallery.set_date_from_photos()
        gallery.make_path()
        gallery.save()
        return gallery

    def create_galleries(self) -> None:
        self.log("Creating galleries:")

        clipper = get_clipper_group().pk
        cof = get_cof_group().pk
        bds = get_bds_group().pk
        exte = User.objects.get(username="exte")

        base = settings.BASE_DIR / "photos_examples"
        vox = base / "vox"
        aerials = base / "aerials"
        formats = base / "formats"

        g = self.mk_gallery(None, "Vide Publique", "vide", None)
        g.public = True
        g.save()

        g = self.mk_gallery("BDS", "Aerials Publique", "ae1", aerials)
        g.public = True
        g.can_view_groups.add(bds)
        g.can_upload_groups.add(bds)
        g.can_delete_groups.add(bds)
        g.can_edit_groups.add(bds)
        g.save()

        g = self.mk_gallery("COF", "Aerials Clipper", "ae2", aerials)
        g.can_view_groups.add(clipper)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        g.save()

        g = self.mk_gallery("COF", "Caché", "ae3", aerials)
        g.can_view_groups.add(clipper)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        g.hidden = True
        g.save()

        # This triggers a warning because the ico image is of nonstandard size
        with catch_warnings(record=True) as w:
            g = self.mk_gallery("Soirées", "Formats", "formats", formats)
            assert len(w) == 1
        g.can_view_groups.add(clipper)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        g.save()

        g = self.mk_gallery("Évènements", "Via clé", "key1", vox)
        g.can_view_groups.add(clipper)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        GalleryPassword(gallery=g, expires=None, key="key0").save()
        g.save()

        g = self.mk_gallery("Évènements", "Via clé expirée", "key2", vox)
        g.can_view_groups.add(clipper)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        GalleryPassword(gallery=g, expires=now() - timedelta(days=1), key="key1").save()
        g.save()

        g = self.mk_gallery("Évènements", "COF seul", "cof-seul", aerials)
        g.can_view_groups.add(cof)
        g.can_upload_groups.add(cof)
        g.can_delete_groups.add(cof)
        g.can_edit_groups.add(cof)
        g.save()

        g = self.mk_gallery("BDS", "BDS + exte", "bds-plus-exte", vox)
        g.can_view_groups.add(bds)
        g.can_upload_groups.add(bds)
        g.can_delete_groups.add(bds)
        g.can_edit_groups.add(bds)
        g.can_view_users.add(exte.pk)
        g.save()

    def handle(self, *args, **kwargs) -> None:
        self.create_groups()
        self.create_users()
        self.create_folders()
        self.create_galleries()
