from authens.signals import post_cas_connect
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from .utils import get_clipper_group, get_tous_group


@receiver(post_cas_connect)
def cas_connection_receiver(created: bool, instance: User, **kwargs) -> None:
    """Add a user to the clipper group when first registering"""
    if created:
        clipper = get_clipper_group()
        instance.groups.add(clipper.pk)


@receiver(post_save, sender=User)
def user_save_receiver(instance: User, created: bool, **kwargs) -> None:
    if created:
        tous = get_tous_group()
        instance.groups.add(tous.pk)
