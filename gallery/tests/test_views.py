from io import BytesIO
from os import listdir, remove
from shutil import copy
from typing import Any

from django.conf import settings
from django.contrib.auth.models import Group, User
from django.urls import reverse

from config.constants import KEY_QUERY_PARAM_NAME

from ..management.commands.make_example_db import PASSWORD
from ..models import Folder, Gallery, GalleryPassword, Photo
from ..utils import get_bds_group, get_cof_group
from ..views.mixins import (
    CHANGE_FOLDER_PERMISSION,
    CREATE_FOLDER_PERMISSION,
    CREATE_GALLERY_PERMISSION,
    DELETE_FOLDER_PERMISSION,
    DELETE_GALLERY_PERMISSION,
    reverse_path,
)
from .base import ViewTstBase
from .utils import (
    GalleryPermissionBackup,
    Perms,
    bool2status,
    bool2status_photo,
    bool2status_redirect,
    create_folder,
    create_gallery,
    pk,
)

FOLDERS: list[Perms] = [
    Perms(
        instance="BDS", no_auth=True, users={"exte", "clipper", "cof", "bds", "admin"}
    ),
    Perms(
        instance="COF", no_auth=True, users={"exte", "clipper", "cof", "bds", "admin"}
    ),
    Perms(
        instance="Vide", no_auth=True, users={"exte", "clipper", "cof", "bds", "admin"}
    ),
    Perms(
        instance="Évènements", users={"clipper", "cof", "bds", "admin"}, keys={"key0"}
    ),
    Perms(instance="Soirées", users={"clipper", "cof", "bds", "admin"}),
    Perms(
        instance="K-Fêt", no_auth=True, users={"exte", "clipper", "cof", "bds", "admin"}
    ),
    Perms(instance="/Not-a-folder"),
]

KEY = "a89kbv5"

GALLERIES: list[Perms] = [
    Perms(
        instance="Vide Publique",
        no_auth=True,
        users={"exte", "clipper", "cof", "bds", "admin"},
    ),
    Perms(
        instance="Aerials Publique",
        no_auth=True,
        users={"exte", "clipper", "cof", "bds", "admin"},
    ),
    Perms(
        instance="Aerials Clipper", users={"clipper", "cof", "bds", "admin"}, keys={KEY}
    ),
    Perms(instance="Caché", users={"cof", "admin"}),
    Perms(instance="Formats", users={"clipper", "cof", "bds", "admin"}),
    Perms(instance="Via clé", users={"clipper", "cof", "bds", "admin"}, keys={"key0"}),
    Perms(instance="Via clé expirée", users={"clipper", "cof", "bds", "admin"}),
    Perms(instance="COF seul", users={"cof", "admin"}),
    Perms(instance="BDS + exte", users={"exte", "bds", "admin"}),
    Perms(instance="Not-a-gallery"),
]


class ViewTests(ViewTstBase):
    """Runs all view tests
    This single class inherits from all the more specific test classes,
    this allows to perform the lengthy DB initialization only once"""

    f_cof: Folder
    f_bds: Folder
    f_kfet: Folder

    g_vide: Gallery
    g_aerials: Gallery
    g_hidden: Gallery
    g_key: Gallery

    p_hidden: Photo
    p_visible: Photo
    p_to_delete: Photo

    gp: GalleryPassword

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.f_cof = Folder.objects.get(name="COF")
        cls.f_bds = Folder.objects.get(name="BDS")
        cls.f_kfet = Folder.objects.get(name="K-Fêt")

        cls.g_vide = Gallery.objects.get(name="Vide Publique")
        cls.g_aerials = Gallery.objects.get(name="Aerials Clipper")
        cls.g_hidden = Gallery.objects.get(name="Caché")
        cls.g_key = Gallery.objects.get(name="Via clé")

        cls.make_user_with_perm("folder_change", CHANGE_FOLDER_PERMISSION)
        cls.make_user_with_perm("folder_create", CREATE_FOLDER_PERMISSION)
        cls.make_user_with_perm("folder_delete", DELETE_FOLDER_PERMISSION)

        cls.make_user_with_perm("gallery_create", CREATE_GALLERY_PERMISSION)

        cls.g_aerials.can_edit_users.add(cls.make_user_with_perm("gallery_edit", None))
        cls.g_aerials.can_edit_users.add(
            cls.make_user_with_perm("gallery_delete", DELETE_GALLERY_PERMISSION)
        )
        cls.g_aerials.can_delete_users.add(
            cls.make_user_with_perm("gallery_photo_delete", None)
        )
        cls.g_aerials.can_upload_users.add(
            cls.make_user_with_perm("gallery_upload", None)
        )
        cls.g_aerials.can_edit_groups.add(get_bds_group().pk)
        cls.g_aerials.save()

        qs = list(cls.g_aerials.photo_set.all())
        cls.p_hidden = qs[0]
        cls.p_hidden.visible = False
        cls.p_hidden.save()
        cls.p_visible = qs[1]
        _ = str(cls.p_visible)

        cls.p_to_delete = qs[2]
        cls.p_to_delete.visible = False
        cls.p_to_delete.save()

        cls.gp = GalleryPassword(gallery=cls.g_aerials, key=KEY, expires=None)
        cls.gp.save()

    # =============================================================
    # § Misc Tests
    # =============================================================

    def test_robots_sitemap(self) -> None:
        response = self.client.get(reverse("robots"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("sitemap"))
        self.assertEqual(response.status_code, 200)

    def test_login_page_no_auth(self) -> None:
        # Not logged in -> valid
        response = self.client.get(reverse("authens:login"))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse("authens:login") + "?next=/dossier/cof/")
        self.assertEqual(response.status_code, 200)

    def test_login_page_auth(self) -> None:
        # Already logged in -> redirect to home
        self.client.login(username="admin", password=PASSWORD)
        response = self.client.get(reverse("authens:login"))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")
        # Redirect to next
        response = self.client.get(reverse("authens:login") + "?next=/dossier/cof/")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/dossier/cof/")
        self.client.logout()

    def test_logout(self) -> None:
        response = self.client.post(reverse("logout"))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")

        self.client.login(username="admin", password=PASSWORD)
        response = self.client.post(reverse("logout"))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")
        self.client.logout()

    def test_gallery_move(self) -> None:
        old = self.g_hidden.path
        self.g_hidden.path = "hidden"
        self.g_hidden.save()

        for photo in self.g_hidden.photo_set.all():
            self.assertRegex(photo.path(), r"^hidden/")
            self.assertTrue((settings.PHOTO_ROOT / photo.path()).is_file())
            self.assertTrue((settings.PHOTO_ROOT / photo.thumbnail_path()).is_file())

        self.g_hidden.path = old
        self.g_hidden.save()

    def test_photo_move_del(self) -> None:
        photo = self.g_hidden.photo_set.all()[0]
        self.g_hidden.preview = photo
        self.g_hidden.save()
        self.f_cof.preview = photo
        self.f_cof.save()
        with self.subTest("Moving within gallery"):
            photo.filename = "new_name.jpg"
            photo.save()
            photo = Photo.objects.get(pk=photo.pk)
            self.assertTrue((settings.PHOTO_ROOT / photo.path()).is_file())
            self.assertTrue((settings.PHOTO_ROOT / photo.thumbnail_path()).is_file())
            self.assertEqual(Gallery.objects.get(pk=self.g_hidden.pk).preview, photo)
            self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, photo)
        self.g_hidden.preview = photo
        self.g_hidden.save()
        self.f_cof.preview = photo
        self.f_cof.save()
        with self.subTest("Moving across galleries"):
            photo.filename = "other_new_name.jpg"
            photo.gallery = self.g_aerials
            photo.save()
            photo = Photo.objects.get(pk=photo.pk)
            self.assertEqual(photo.gallery, self.g_aerials)
            self.assertTrue((settings.PHOTO_ROOT / photo.path()).is_file())
            self.assertTrue((settings.PHOTO_ROOT / photo.thumbnail_path()).is_file())
            self.assertEqual(Gallery.objects.get(pk=self.g_hidden.pk).preview, None)
            self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)
        self.g_hidden.preview = None
        self.g_hidden.save()
        self.f_cof.preview = None
        self.f_cof.save()
        with self.subTest("Deleting one photo"):
            photo.delete()
            self.assertFalse((settings.PHOTO_ROOT / photo.path()).is_file())
            self.assertFalse((settings.PHOTO_ROOT / photo.thumbnail_path()).is_file())
        with self.subTest("Deleting queryset"):
            self.g_hidden.photo_set.all().delete()
            self.assertEqual(
                listdir(
                    settings.PHOTO_ROOT
                    / settings.PHOTO_PATH.format(GALLERY_PATH=self.g_hidden.path)
                ),
                [],
            )
            self.assertEqual(
                listdir(
                    settings.PHOTO_ROOT
                    / settings.THUMBNAIL_PATH.format(GALLERY_PATH=self.g_hidden.path)
                ),
                [],
            )

    def test_photo_upload(self) -> None:
        perm = Perms(users={"admin", "cof", "gallery_upload"})
        url = reverse_path("photo_upload", self.g_aerials)
        data_valid: list[dict[str, Any]] = [
            {"filename": "hello_there"},
            {},
        ]
        data_invalid: list[dict[str, Any]] = [
            {},
            {"file": BytesIO(b"Not a file")},
        ]

        def tst(perm: bool, _keys: set[str], user: User | None):
            response = self.client.get(url)
            self.assertEqual(response.status_code, bool2status(perm, user))

            photo_count = self.g_aerials.photo_set.all().count()

            for data in data_valid:
                with self.subTest(data=data):
                    with open(settings.PHOTO_ROOT / self.p_hidden.path(), "rb") as file:
                        response = self.client.post(url, data=data | {"file": file})
                    self.assertEqual(response.status_code, bool2status(perm, user))
                    new_count = self.g_aerials.photo_set.all().count()
                    if not perm:
                        self.assertEqual(new_count, photo_count)
                    else:
                        self.assertJSONEqual(
                            response.content.decode(),
                            '{"status":"ok"}',
                        )
                        self.assertEqual(new_count, photo_count + 1)
                        photo_count += 1

            photo_count = self.g_aerials.photo_set.all().count()

            for data in data_invalid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data)
                    self.assertEqual(response.status_code, bool2status(perm, user))
                    self.assertEqual(
                        self.g_aerials.photo_set.all().count(), photo_count
                    )
                    if perm:
                        self.assertJSONNotEqual(
                            response.content.decode(),
                            '{"status":"ok"}',
                        )

        self.iter_users(perm, tst)

    # =============================================================
    # § Folder Tests
    # =============================================================

    def test_home(self) -> None:
        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            """perm is ignored since anyone can access home"""
            response = self.client.get(reverse("home"))
            self.assertEqual(response.status_code, 200)
            if user is None:
                self.assertContains(response, '<ul class="bubble"><li class="warning">')

        self.iter_users(Perms(), tst)

    def test_folder_view(self) -> None:
        for perm in FOLDERS:
            with self.subTest(perm=perm):
                try:
                    folder = Folder.objects.get(name=perm.instance)
                    url = reverse_path("folder", folder)
                except Folder.DoesNotExist:
                    url = reverse("folder", kwargs={"path": perm.instance})

                def tst(get: bool, keys: set[str], user: User | None) -> None:
                    """Test a query with all keys"""
                    response = self.client.get(url)
                    self.assertEqual(
                        response.status_code, bool2status(get or perm.no_auth, user)
                    )
                    for key in self.keys:
                        fmt_key = "?{}={}".format(KEY_QUERY_PARAM_NAME, key)
                        response = self.client.get(url + fmt_key)
                        expected = get or (key in keys) or perm.no_auth
                        self.assertEqual(
                            response.status_code, bool2status(expected, user)
                        )

                self.iter_users(perm, tst)

    def test_redirect_to_home(self):
        """Redirect to home when going to view folder with path '/'"""
        response = self.client.get(reverse("folder", kwargs={"path": "/"}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/")

    def test_folder_create(self):
        url1 = reverse("folder_new", kwargs={"path": "/"})
        url2 = reverse_path("folder_new", self.f_cof)
        perm = Perms(users={"cof", "bds", "admin", "folder_create"})
        name = "paf"
        data_valid: list[dict[str, Any]] = [
            {"parent": "", "name": name, "slug": "paf"},
            {"parent": pk(self.f_cof), "name": name, "slug": "paf"},
        ]
        data_invalid: list[dict[str, Any]] = [
            {"parent": "pif", "name": "paf", "slug": "paf"},
            {"parent": "1", "name": "", "slug": "paf"},
            {"parent": "", "name": "Paf", "slug": ""},
            {"parent": "1", "name": "", "slug": "Not a slug"},
            {
                "parent": pk(self.f_cof.parent),
                "name": "Paf",
                "slug": self.f_cof.slug,
            },  # slug already taken
        ]

        folder_count = Folder.objects.all().count()

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            for url in (url1, url2):
                response = self.client.get(url)
                self.assertEqual(response.status_code, bool2status(perm, user))

                for data in data_valid:
                    with self.subTest(data=data, url=url):
                        response = self.client.post(url, data=data)
                        self.assertEqual(
                            response.status_code, bool2status_redirect(perm, user)
                        )
                        if not perm:
                            self.assertEqual(Folder.objects.all().count(), folder_count)
                        else:
                            self.assertEqual(
                                Folder.objects.all().count(), folder_count + 1
                            )
                            folder = Folder.objects.get(name=name)
                            self.assertRedirects(
                                response,
                                reverse_path("folder", folder),
                            )
                            folder.delete()

                for data in data_invalid:
                    with self.subTest(data=data, url=url):
                        response = self.client.post(url, data=data)
                        self.assertEqual(response.status_code, bool2status(perm, user))
                        self.assertEqual(Folder.objects.all().count(), folder_count)

        self.iter_users(perm, tst)

    def test_folder_edit(self):
        url_invalid = reverse("folder_edit", kwargs={"path": "/"})
        url = reverse_path("folder_edit", self.f_cof)
        perm = Perms(users={"folder_change", "admin", "cof", "bds"})
        name = "paf"
        data_valid: list[dict[str, Any]] = [
            {"parent": "", "name": name, "slug": "cof"},
            {"parent": pk(self.f_bds), "name": "COF", "slug": "paf"},
            {"parent": "", "name": self.f_cof.name, "slug": self.f_cof.slug},
        ]
        data_invalid: list[dict[str, Any]] = [
            {"parent": "pif", "name": name, "slug": "paf"},
            {"parent": "", "name": "", "slug": "paf"},
            {"parent": "", "name": "Paf", "slug": ""},
            {"parent": "", "name": "", "slug": "Not a slug"},
            {
                "parent": pk(self.f_bds.parent),
                "name": "Paf",
                "slug": self.f_bds.slug,
            },  # slug bds already taken
            {
                "parent": pk(self.f_cof),
                "name": "Paf",
                "slug": "cof",
            },  # child of itself
            {
                "parent": pk(self.f_kfet),
                "name": "Paf",
                "slug": "cof",
            },  # grandchild of itself
        ]

        folder_count = Folder.objects.all().count()

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(url_invalid)
            self.assertEqual(response.status_code, 404 if user is not None else 302)

            response = self.client.get(url)
            self.assertEqual(response.status_code, bool2status(perm, user))

            for data in data_valid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data)
                    self.assertEqual(
                        response.status_code, bool2status_redirect(perm, user)
                    )
                    self.assertEqual(Folder.objects.all().count(), folder_count)
                    new = Folder.objects.get(pk=self.f_cof.pk)
                    if not perm:
                        self.assertEqual(new, self.f_cof)
                    else:
                        self.assertEqual(new.name, data["name"])
                        self.assertEqual(new.slug, data["slug"])
                        self.assertRedirects(
                            response,
                            reverse_path("folder", new),
                        )
                    self.f_cof.save()

            for data in data_invalid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data)
                    self.assertEqual(response.status_code, bool2status(perm, user))
                    self.assertEqual(Folder.objects.all().count(), folder_count)
                    self.assertEqual(Folder.objects.get(pk=self.f_cof.pk), self.f_cof)
                    if perm:
                        self.assertErrorMsg(response)
                    self.f_cof.save()

        self.iter_users(perm, tst)

    def test_folder_delete(self):
        url_invalid = reverse("folder_delete", kwargs={"path": "/"})
        perm = Perms(users={"folder_delete", "admin", "cof", "bds"})

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            # Can't delete root
            response = self.client.get(url_invalid)
            self.assertEqual(response.status_code, 404 if user else 302)

            # Create two folders for deletion
            f1 = create_folder(None, "f1", "del")
            f2 = create_folder("COF", "f2", "del")

            folder_count = Folder.objects.all().count()

            response1 = self.client.get(reverse_path("folder_delete", f1))
            self.assertEqual(response1.status_code, bool2status_redirect(perm, user))

            response2 = self.client.get(reverse_path("folder_delete", f2))
            self.assertEqual(response2.status_code, bool2status_redirect(perm, user))

            response3 = self.client.get(reverse_path("folder_delete", self.f_cof))
            self.assertEqual(response3.status_code, bool2status_redirect(perm, user))

            count = Folder.objects.all().count()
            if perm:
                self.assertRedirects(response1, reverse("home"))
                self.assertRedirects(response2, reverse_path("folder", f2.parent))
                self.assertRedirects(response3, reverse_path("folder", self.f_cof))
                self.assertEqual(count, folder_count - 2)
                self.assertRaises(
                    Folder.DoesNotExist, lambda: Folder.objects.get(name="f1")
                )
                self.assertRaises(
                    Folder.DoesNotExist, lambda: Folder.objects.get(name="f2")
                )
            else:
                self.assertEqual(count, folder_count)
                Folder.objects.get(name="f1")
                Folder.objects.get(name="f2")
                f1.delete()
                f2.delete()
            # Non empty is undeleted
            Folder.objects.get(name="COF")

        self.iter_users(perm, tst)

    def test_folder_preview(self):
        kwargs1 = {"gallery": self.g_aerials.pk, "filename": self.p_visible.filename}
        kwargs2 = {"gallery": self.g_aerials.pk, "filename": self.p_hidden.filename}

        perm = Perms(users={"folder_change", "admin", "cof", "bds"})

        set_f = reverse("folder_preview_set", kwargs={"path": self.f_cof} | kwargs1)
        set_f1 = reverse("folder_preview_set", kwargs={"path": self.f_cof} | kwargs2)
        set_f2 = reverse("folder_preview_set", kwargs={"path": self.f_bds} | kwargs1)
        set_f3 = reverse("folder_preview_set", kwargs={"path": self.f_bds} | kwargs2)
        unset_f = reverse_path("folder_preview_del", self.f_cof)

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(set_f)
            folder = Folder.objects.get(pk=self.f_cof.pk)
            with self.subTest("Setting preview"):
                if perm:
                    self.assertRedirects(response, reverse_path("folder", self.f_cof))
                    self.assertEqual(folder.preview, self.p_visible)
                    if user is not None and self.g_aerials.can_view(user, ""):
                        self.assertEqual(
                            folder.dynamic_preview(user, ""), self.p_visible
                        )
                else:
                    self.assertEqual(response.status_code, 302 if user is None else 404)
                    self.assertEqual(folder.preview, None)
            self.f_cof.save()

            with self.subTest("Invalid set (hidden photo/non-existant photo)"):
                response = self.client.get(set_f1)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)
                response = self.client.get(set_f2)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)
                response = self.client.get(set_f3)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)

            with self.subTest("Can't unset if not set"):
                response = self.client.get(unset_f)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)

            self.f_cof.preview = self.p_visible
            self.f_cof.save()
            with self.subTest("Unset preview"):
                response = self.client.get(unset_f)
                if perm:
                    self.assertRedirects(
                        response, reverse_path("folder_edit", self.f_cof)
                    )
                    self.assertEqual(Folder.objects.get(pk=self.f_cof.pk).preview, None)
                else:
                    self.assertEqual(response.status_code, 302 if user is None else 404)
                    self.assertEqual(
                        Folder.objects.get(pk=self.f_cof.pk).preview, self.p_visible
                    )
            self.f_cof.preview = None
            self.f_cof.save()

        self.iter_users(perm, tst)

    # =============================================================
    # § Gallery Tests
    # =============================================================

    def test_gallery_view(self) -> None:
        for perm in GALLERIES:
            with self.subTest(perm=perm):
                url_photo = None
                url_thumbnail = None
                try:
                    gallery = Gallery.objects.get(name=perm.instance)
                    url = reverse_path("gallery", gallery)
                    qs = list(gallery.photo_set.filter(visible=True))
                    if qs:
                        photo = qs[0]
                        kwargs = {"gallery": gallery.id, "name": photo.filename}
                        url_photo = reverse("photo", kwargs=kwargs)
                        url_thumbnail = reverse("photo", kwargs=kwargs)
                except Gallery.DoesNotExist:
                    url = reverse("gallery", kwargs={"path": "/not/a/gallery"})

                def tst(get: bool, keys: set[str], user: User | None) -> None:
                    """Test a query with all keys"""
                    response = self.client.get(url)
                    self.assertEqual(
                        response.status_code, bool2status(get or perm.no_auth, user)
                    )
                    if url_photo is not None:
                        response = self.client.get(url_photo)
                        self.assertEqual(
                            response.status_code,
                            bool2status_photo(get or perm.no_auth, user),
                        )
                    if url_thumbnail is not None:
                        response = self.client.get(url_thumbnail)
                        self.assertEqual(
                            response.status_code,
                            bool2status_photo(get or perm.no_auth, user),
                        )

                    for key in self.keys:
                        fmt_key = "?{}={}".format(KEY_QUERY_PARAM_NAME, key)
                        response = self.client.get(url + fmt_key)
                        expected = get or perm.no_auth or (key in keys)
                        self.assertEqual(
                            response.status_code, bool2status(expected, user)
                        )

                        if url_photo is not None:
                            response = self.client.get(url_photo + fmt_key)
                            self.assertEqual(
                                response.status_code,
                                bool2status_photo(expected, user),
                            )
                        if url_thumbnail is not None:
                            response = self.client.get(url_thumbnail + fmt_key)
                            self.assertEqual(
                                response.status_code,
                                bool2status_photo(expected, user),
                            )

                self.iter_users(perm, tst)

    def test_gallery_create(self):
        url1 = reverse("gallery_new", kwargs={"path": "/"})
        url2 = reverse_path("gallery_new", Folder.objects.get(name="COF"))
        perm = Perms(users={"cof", "bds", "admin", "gallery_create"})
        name = "paf"
        data_valid: list[dict[str, Any]] = [
            {"parent": "", "name": name, "slug": "paf"},
            {"parent": "1", "name": name, "slug": "paf"},
        ]
        data_invalid: list[dict[str, Any]] = [
            {"parent": "pif", "name": name, "slug": "paf"},
            {"parent": "1", "name": "", "slug": "paf"},
            {"parent": "", "name": "Paf", "slug": ""},
            {"parent": "1", "name": "", "slug": "Not a slug"},
            {
                "parent": pk(self.g_vide.parent),
                "name": "Paf",
                "slug": self.g_vide.slug,
            },  # slug already taken
        ]

        gallery_count = Gallery.objects.all().count()

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            for url in (url1, url2):
                response = self.client.get(url)
                self.assertEqual(response.status_code, bool2status(perm, user))

                for data in data_valid:
                    with self.subTest(data=data, url=url):
                        response = self.client.post(url, data=data)
                        self.assertEqual(
                            response.status_code, bool2status_redirect(perm, user)
                        )
                        if not perm:
                            self.assertEqual(
                                Gallery.objects.all().count(), gallery_count
                            )
                        else:
                            assert user is not None
                            self.assertEqual(
                                Gallery.objects.all().count(), gallery_count + 1
                            )
                            gallery = Gallery.objects.get(name=name)
                            self.assertRedirects(
                                response,
                                reverse_path("gallery_perms", gallery),
                            )
                            # Creator all permissions on new gallery
                            self.assertTrue(gallery.can_view(user, None))
                            self.assertTrue(gallery.can_upload(user))
                            self.assertTrue(gallery.can_delete(user))
                            self.assertTrue(gallery.can_edit(user))
                            gallery.delete()

                for data in data_invalid:
                    with self.subTest(data=data, url=url):
                        response = self.client.post(url, data=data)
                        self.assertEqual(response.status_code, bool2status(perm, user))
                        self.assertEqual(Gallery.objects.all().count(), gallery_count)
                        if perm:
                            self.assertErrorMsg(response)

        self.iter_users(perm, tst)

    def test_gallery_edit(self):
        url = reverse_path("gallery_edit", self.g_aerials)
        perm = Perms(users={"gallery_edit", "gallery_delete", "cof", "admin", "bds"})
        data_valid: list[dict[str, Any]] = [
            {"parent": "", "name": "paf", "slug": "pistache-farcie"},
            {
                "parent": pk(self.f_cof.parent),
                "name": self.f_cof.name,
                "slug": self.f_cof.slug,
            },  # No conflict with a folder
            {
                "parent": pk(self.g_aerials.parent),
                "name": self.g_aerials.name,
                "slug": self.g_aerials.slug,
            },
        ]
        data_invalid: list[dict[str, Any]] = [
            {"parent": "pif", "name": "pif", "slug": "paf"},
            {"parent": "", "name": "", "slug": "paf"},
            {"parent": "", "name": "Paf", "slug": ""},
            {"parent": "", "name": "", "slug": "Not a slug"},
            {
                "parent": pk(self.g_hidden.parent),
                "name": "Paf",
                "slug": self.g_hidden.slug,
            },  # conflict
        ]

        gallery_count = Gallery.objects.all().count()

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(url)
            self.assertEqual(response.status_code, bool2status(perm, user))

            for data in data_valid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data)
                    self.assertEqual(
                        response.status_code, bool2status_redirect(perm, user)
                    )
                    self.assertEqual(Gallery.objects.all().count(), gallery_count)
                    new = Gallery.objects.get(id=self.g_aerials.pk)
                    if not perm:
                        self.assertEqual(new, self.g_aerials)
                    else:
                        self.assertEqual(new.name, data["name"])
                        self.assertEqual(new.slug, data["slug"])
                        self.assertRedirects(
                            response,
                            reverse_path("gallery_manage", new),
                        )
                    self.g_aerials.save()

            for data in data_invalid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data)
                    self.assertEqual(response.status_code, bool2status(perm, user))
                    self.assertEqual(Gallery.objects.all().count(), gallery_count)
                    self.assertEqual(
                        Gallery.objects.get(id=self.g_aerials.pk), self.g_aerials
                    )
                    if perm:
                        self.assertErrorMsg(response)
                    self.g_aerials.save()

        self.iter_users(perm, tst)

    def test_gallery_delete(self):
        perm = Perms(users={"gallery_delete", "admin", "cof", "bds"})
        cof = get_cof_group().pk
        bds = get_bds_group().pk

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            # Create two galleries for deletion
            f1 = create_gallery(None, "f1", "del")
            f2 = create_gallery("COF", "f2", "del")

            f1.can_edit_groups.add(cof)
            f1.can_edit_groups.add(bds)
            f2.can_edit_groups.add(cof)
            f2.can_edit_groups.add(bds)

            # Delete permission is local can_edit + global can_delete_galleries
            if user is not None and user.username == "gallery_delete":
                f1.can_edit_users.add(user)
                f2.can_edit_users.add(user)
            f1.save()
            f2.save()

            gallery_count = Gallery.objects.all().count()
            response1 = self.client.get(reverse_path("gallery_delete", f1))
            self.assertEqual(response1.status_code, bool2status_redirect(perm, user))

            response2 = self.client.get(reverse_path("gallery_delete", f2))
            self.assertEqual(response2.status_code, bool2status_redirect(perm, user))

            response3 = self.client.get(reverse_path("gallery_delete", self.g_aerials))
            self.assertEqual(response3.status_code, bool2status_redirect(perm, user))

            for photo in self.g_aerials.photo_set.all():
                photo.visible = False
                photo.save()
            response4 = self.client.get(reverse_path("gallery_delete", self.g_aerials))
            self.assertEqual(response4.status_code, bool2status_redirect(perm, user))
            for photo in self.g_aerials.photo_set.all():
                photo.visible = True
                photo.save()

            count = Gallery.objects.all().count()
            if perm:
                self.assertRedirects(response1, reverse("home"))
                self.assertRedirects(response2, reverse_path("folder", f2.parent))
                self.assertRedirects(
                    response3, reverse_path("gallery_manage", self.g_aerials)
                )
                self.assertRedirects(
                    response4, reverse_path("gallery_manage", self.g_aerials)
                )
                self.assertEqual(count, gallery_count - 2)
                self.assertRaises(
                    Gallery.DoesNotExist, lambda: Gallery.objects.get(name="f1")
                )
                self.assertRaises(
                    Gallery.DoesNotExist, lambda: Gallery.objects.get(name="f2")
                )
            else:
                self.assertEqual(count, gallery_count)
                self.assertEqual(Gallery.objects.get(name="f1"), f1)
                self.assertEqual(Gallery.objects.get(name="f2"), f2)
            f1.delete()
            f2.delete()
            # Non empty is undeleted
            Gallery.objects.get(pk=self.g_aerials.pk)

        self.iter_users(perm, tst)

    def test_gallery_manage(self):
        perm = Perms(
            users={
                "gallery_delete",
                "admin",
                "cof",
                "bds",
                "gallery_photo_delete",
                "gallery_edit",
            }
        )

        photo_deleted_header = "<h2>Photos supprimées</h2>"
        kwargs = {"gallery": self.g_aerials.pk, "name": self.p_hidden.filename}
        url_photo = reverse("photo", kwargs=kwargs)
        url_thumbnail = reverse("photo", kwargs=kwargs)

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(reverse_path("gallery_manage", self.g_aerials))
            self.assertEqual(response.status_code, bool2status(perm, user))

            response_photo = self.client.get(url_photo)
            response_thumbnail = self.client.get(url_thumbnail)

            can_edit = user is not None and user.username in {
                "admin",
                "cof",
                "bds",
                "gallery_delete",
                "gallery_edit",
            }

            self.assertEqual(
                response_photo.status_code, bool2status_photo(can_edit, user)
            )
            self.assertEqual(
                response_thumbnail.status_code, bool2status_photo(can_edit, user)
            )

            if perm:
                if can_edit:
                    self.assertContains(response, photo_deleted_header)
                else:
                    self.assertNotContains(response, photo_deleted_header)

        self.iter_users(perm, tst)

    def test_photo_hide(self):
        url_delete = reverse(
            "delete_photo",
            kwargs={"gallery": self.g_aerials.pk, "name": self.p_visible.filename},
        )
        url_restore = reverse(
            "delete_photo",
            kwargs={"gallery": self.g_aerials.pk, "name": self.p_hidden.filename},
        )

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response_delete = self.client.get(url_delete)
            response_restore = self.client.get(url_restore)

            can_edit = user is not None and user.username in {
                "admin",
                "cof",
                "bds",
                "gallery_delete",
                "gallery_edit",
            }
            can_delete = user is not None and user.username in {
                "admin",
                "cof",
                "gallery_photo_delete",
            }

            self.assertEqual(response_delete.status_code, 302 if can_delete else 404)
            self.assertEqual(
                not can_delete, Photo.objects.get(pk=self.p_visible.pk).visible
            )
            self.assertEqual(response_restore.status_code, 302 if can_edit else 404)
            self.assertEqual(can_edit, Photo.objects.get(pk=self.p_hidden.pk).visible)

            self.p_hidden.save()
            self.p_visible.save()

        self.iter_users(Perms(), tst)

    def test_photo_delete_permanent(self):
        perm = Perms(
            users={
                "admin",
                "cof",
                "bds",
                "gallery_delete",
                "gallery_edit",
            }
        )
        url = reverse(
            "delete_photo_permanent",
            kwargs={"gallery": self.g_aerials.pk, "name": self.p_to_delete.filename},
        )
        url_invalid = reverse(
            "delete_photo_permanent",
            kwargs={"gallery": self.g_aerials.pk, "name": self.p_visible.filename},
        )

        path_photo = settings.PHOTO_ROOT / self.p_to_delete.path()
        path_thumb = settings.PHOTO_ROOT / self.p_to_delete.thumbnail_path()
        path_visible = settings.PHOTO_ROOT / self.p_visible.path()
        path_visible_thumb = settings.PHOTO_ROOT / self.p_visible.thumbnail_path()
        path_backup = settings.PHOTO_ROOT / "backup.jpg"
        path_backup_thumbs = settings.PHOTO_ROOT / "backup_thumbs.jpg"
        copy(path_photo, path_backup)
        copy(path_thumb, path_backup_thumbs)

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(url)
            response_invalid = self.client.get(url_invalid)

            with self.subTest("Testing deletions"):
                self.assertEqual(response.status_code, 302 if perm else 404)
                self.assertEqual(path_photo.is_file(), not perm)
                self.assertEqual(path_thumb.is_file(), not perm)
                if perm:
                    self.assertRaises(
                        Photo.DoesNotExist,
                        lambda: Photo.objects.get(pk=self.p_to_delete.pk),
                    )
                else:
                    Photo.objects.get(pk=self.p_to_delete.pk)
                self.assertEqual(response_invalid.status_code, 404)
                self.assertTrue(path_visible.is_file())
                self.assertTrue(path_visible_thumb.is_file())
                Photo.objects.get(pk=self.p_visible.pk)
            copy(path_backup, path_photo)
            copy(path_backup_thumbs, path_thumb)
            self.p_to_delete.save()
            self.p_visible.save()

        self.iter_users(perm, tst)
        remove(path_backup)
        remove(path_backup_thumbs)

    def test_gallery_perm(self):
        url = reverse_path("gallery_perms", self.g_aerials)
        perm = Perms(users={"gallery_edit", "gallery_delete", "cof", "admin", "bds"})
        data_prefix = {
            "form_group-TOTAL_FORMS": "6",
            "form_group-INITIAL_FORMS": "6",
            "form_group-MIN_NUM_FORMS": "6",
            "form_group-MAX_NUM_FORMS": "6",
            "form_user-TOTAL_FORMS": "2",
            "form_user-INITIAL_FORMS": "0",
            "form_user-MIN_NUM_FORMS": "0",
            "form_user-MAX_NUM_FORMS": "1000",
            "form_group-0-group": "2",  # Tous
            "form_group-1-group": "1",  # Clipper
            "form_group-2-group": "4",  # Cof
            "form_group-3-group": "3",  # BDS
            "form_group-4-group": "5",
            "form_group-5-group": "6",
            "key_form-TOTAL_FORMS": "1",
            "key_form-INITIAL_FORMS": "1",
            "key_form-MIN_NUM_FORMS": "1",
            "key_form-MAX_NUM_FORMS": "1",
            "new_key": "",
            "form_user-0-user": "",
            "form_user-1-user": "",
            "key_form-0-id": "{}".format(self.gp.pk),
        }

        # We need to save and restore these fields
        backup = GalleryPermissionBackup.save(self.g_aerials)
        data_valid: list[dict[str, Any]] = [
            {},
            {"public": "on"},
            {"hidden": "on", "public": "gibberish"},
            {
                "form_group-0-can_view": "on",
                "form_group-1-can_upload": "on",
                "form_group-2-can_delete": "on",
                "form_group-3-can_edit": "on",
                "form_group-4-can_view": "on",
                "form_group-4-can_upload": "on",
                "form_group-4-can_delete": "on",
                "form_group-4-can_edit": "on",
            },
            {
                "form_user-0-user": "admin",
                "form_user-0-can_view": "on",
                "form_user-0-can_upload": "on",
                "form_user-1-user": "cof",
                "form_user-1-can_delete": "",
                "form_user-2-user": "bds",
                "form_user-2-can_edit": "on",
                "form_user-2-can_delete": "on",
                "form_user-3-user": "",
                "form_user-TOTAL_FORMS": "4",
                "key_form-0-expires": "",
            },
            {"key_form-0-expires": "2022-10-14"},
            {"new_key": "True"},
        ]
        data_invalid: list[dict[str, Any]] = [
            {"form_user-TOTAL_FORMS": "not-a-number"},
            {"form_user-0-user": "not-a-user"},
            {"form_group-0-group": "Not a number"},
            {"form_user-0-can_view": "on"},  # Checkbox with no user
            {"form_user-0-user": "admin", "form_user-1-user": "admin"},
            {"form_group-0-group": "0"},
            {"form_group-0-group": "1024"},
            {"form_group-0-group": ""},
            {"key_form-0-expires": "Not-a-date"},
            {"key_form-0-id": "{}".format(GalleryPassword.objects.get(key="key0"))},
        ]

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            response = self.client.get(url)
            self.assertEqual(response.status_code, bool2status(perm, user))

            for data in data_valid:
                with self.subTest(data=data):
                    data = data_prefix | data
                    response = self.client.post(url, data=data)
                    self.assertEqual(
                        response.status_code, bool2status_redirect(perm, user)
                    )
                    gallery = Gallery.objects.get(pk=self.g_aerials.pk)
                    if not perm:
                        self.assertEqual(gallery, self.g_aerials)
                        self.assertEqual(backup, GalleryPermissionBackup.save(gallery))
                    else:
                        self.assertEqual(gallery.public, bool(data.get("public")))
                        self.assertEqual(gallery.hidden, bool(data.get("hidden")))
                        for id in range(int(data["form_group-TOTAL_FORMS"])):
                            group = Group.objects.get(
                                pk=data["form_group-{}-group".format(id)]
                            )
                            self.assertEqual(
                                group in gallery.can_view_groups.all(),
                                bool(data.get("form_group-{}-can_view".format(id))),
                            )
                            self.assertEqual(
                                group in gallery.can_upload_groups.all(),
                                bool(data.get("form_group-{}-can_upload".format(id))),
                            )
                            self.assertEqual(
                                group in gallery.can_delete_groups.all(),
                                bool(data.get("form_group-{}-can_delete".format(id))),
                            )
                            self.assertEqual(
                                group in gallery.can_edit_groups.all(),
                                bool(data.get("form_group-{}-can_edit".format(id))),
                            )
                        for id in range(int(data["form_user-TOTAL_FORMS"])):
                            username = data["form_user-{}-user".format(id)]
                            if not username:
                                continue
                            user = User.objects.get(username=username)
                            self.assertEqual(
                                user in gallery.can_view_users.all(),
                                bool(data.get("form_user-{}-can_view".format(id))),
                            )
                            self.assertEqual(
                                user in gallery.can_upload_users.all(),
                                bool(data.get("form_user-{}-can_upload".format(id))),
                            )
                            self.assertEqual(
                                user in gallery.can_delete_users.all(),
                                bool(data.get("form_user-{}-can_delete".format(id))),
                            )
                            self.assertEqual(
                                user in gallery.can_edit_users.all(),
                                bool(data.get("form_user-{}-can_edit".format(id))),
                            )
                        gp = GalleryPassword.objects.get(pk=self.gp.pk)
                        if gp.expires is None:
                            self.assertFalse(bool(data.get("key_form-0-expires")))
                        else:
                            self.assertEqual(
                                gp.expires.strftime("%Y-%m-%d"),
                                data.get("key_form-0-expires"),
                            )
                        all_pwd = list(
                            GalleryPassword.objects.filter(gallery=self.g_aerials)
                        )
                        self.assertIn(gp, all_pwd)
                        if data.get("new_key") == "True":
                            self.assertEqual(len(all_pwd), 2)
                        else:
                            self.assertEqual(len(all_pwd), 1)

                backup.restore(self.g_aerials)
                self.g_aerials.save()

            for data in data_invalid:
                with self.subTest(data=data):
                    response = self.client.post(url, data=data_prefix | data)
                    self.assertEqual(response.status_code, bool2status(perm, user))
                    gallery = Gallery.objects.get(pk=self.g_aerials.pk)
                    self.assertEqual(gallery, self.g_aerials)
                    self.assertEqual(backup, GalleryPermissionBackup.save(gallery))
                    if perm:
                        self.assertErrorMsg(response)
                backup.restore(self.g_aerials)
                self.g_aerials.save()

        self.iter_users(perm, tst)

    def test_gallery_preview(self):
        perm = Perms(users={"gallery_edit", "gallery_delete", "cof", "admin", "bds"})

        set_g = reverse(
            "gallery_preview_set",
            kwargs={"path": self.g_aerials, "filename": self.p_visible.filename},
        )
        set_g1 = reverse(
            "gallery_preview_set",
            kwargs={"path": self.g_aerials, "filename": self.p_hidden.filename},
        )
        set_g2 = reverse(
            "gallery_preview_set",
            kwargs={"path": self.g_vide, "filename": self.p_visible.filename},
        )
        unset_g = reverse_path("gallery_preview_del", self.g_aerials)
        unset_g2 = reverse_path("gallery_preview_del2", self.g_aerials)

        def tst(perm: bool, _keys: set[str], user: User | None) -> None:
            with self.subTest("Setting preview"):
                response = self.client.get(set_g)
                gallery = Gallery.objects.get(pk=self.g_aerials.pk)
                if perm:
                    self.assertRedirects(
                        response, reverse_path("gallery_manage", self.g_aerials)
                    )
                    self.assertEqual(gallery.preview, self.p_visible)
                    self.assertEqual(gallery.dynamic_preview(), self.p_visible)
                else:
                    self.assertEqual(response.status_code, 302 if user is None else 404)
                    self.assertEqual(gallery.preview, None)

            self.g_aerials.save()

            with self.subTest("Invalid set (hidden photo/non-existant photo)"):
                response = self.client.get(set_g1)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(
                    Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                )
                response = self.client.get(set_g2)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(
                    Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                )

            with self.subTest("Can't unset if not set"):
                response = self.client.get(unset_g)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(
                    Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                )
                response = self.client.get(unset_g2)
                self.assertEqual(response.status_code, 302 if user is None else 404)
                self.assertEqual(
                    Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                )

            with self.subTest("Valid unsets"):
                self.g_aerials.preview = self.p_visible
                self.g_aerials.save()
                response = self.client.get(unset_g)
                if perm:
                    self.assertRedirects(
                        response, reverse_path("gallery_edit", self.g_aerials)
                    )
                    self.assertEqual(
                        Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                    )
                else:
                    self.assertEqual(response.status_code, 302 if user is None else 404)
                    self.assertEqual(
                        Gallery.objects.get(pk=self.g_aerials.pk).preview,
                        self.p_visible,
                    )
                self.g_aerials.save()
                response = self.client.get(unset_g2)
                if perm:
                    self.assertEqual(
                        Gallery.objects.get(pk=self.g_aerials.pk).preview, None
                    )
                    self.assertRedirects(
                        response, reverse_path("gallery_manage", self.g_aerials)
                    )
                else:
                    self.assertEqual(response.status_code, 302 if user is None else 404)
                    self.assertEqual(
                        Gallery.objects.get(pk=self.g_aerials.pk).preview,
                        self.p_visible,
                    )
            self.g_aerials.preview = None
            self.g_aerials.save()

        self.iter_users(perm, tst)

    def test_gallery_zip(self):
        perm = Perms(users={"clipper", "cof", "bds", "admin"}, keys={"key0"})
        url = reverse_path("zip", self.g_key)

        def tst(perm: bool, keys: set[str], user: User | None):
            response = self.client.get(url)
            self.assertEqual(response.status_code, bool2status(perm, user))

            if not perm:
                for key in self.keys:
                    response = self.client.get(
                        url + "?{}={}".format(KEY_QUERY_PARAM_NAME, key)
                    )
                    self.assertEqual(
                        response.status_code, bool2status(key in keys, user)
                    )

        self.iter_users(perm, tst)
