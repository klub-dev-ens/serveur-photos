from typing import NamedTuple

from django.test import TestCase

from ..utils import ensure_unique, get_permission
from ..views.mixins import (
    CHANGE_FOLDER_PERMISSION,
    CREATE_FOLDER_PERMISSION,
    CREATE_GALLERY_PERMISSION,
    DELETE_FOLDER_PERMISSION,
    DELETE_GALLERY_PERMISSION,
)


class EnsureUniqueTest(NamedTuple):
    string: str
    not_in: list[str]
    result: tuple[str, str]
    suffix: str = ".jpg"


class TestUtils(TestCase):
    """unit test for utility functions"""

    def test_ensure_unique(self) -> None:
        tests: list[EnsureUniqueTest] = [
            EnsureUniqueTest("hello.jpg", [], ("hello", ".jpg")),
            EnsureUniqueTest("/home/path/hello.jpg", [], ("hello", ".jpg")),
            EnsureUniqueTest(".hello", [], (".hello", "")),
            EnsureUniqueTest("/home/.hello", [], (".hello", "")),
            EnsureUniqueTest("/home/.hello.", [], (".hello", ".")),
            EnsureUniqueTest("a.jpg", ["a.jpg", "a_1"], ("a_1", ".jpg")),
            EnsureUniqueTest("/home/a.jpg", ["bla/a.jpg", "a_1"], ("a_1", ".jpg")),
            EnsureUniqueTest(".a.jpg", ["bla/.a.jpg", "a_1"], (".a_1", ".jpg")),
            EnsureUniqueTest("a.j", ["a.jpg", "a_1.jpg", "a_2.jpg"], ("a_3", ".j")),
            EnsureUniqueTest("/path/to/", [], ("to", "")),
            EnsureUniqueTest("a.b", ["a.jpg"], ("a_1", ".b")),
            EnsureUniqueTest(
                "2022-10-12-del",
                [
                    "2022-10-12-vide",
                    "2022-10-12-ae1",
                    "2022-10-12-ae2",
                    "2022-10-12-ae3",
                    "2022-10-12-formats",
                    "2022-10-12-key1",
                    "2022-10-12-key2",
                    "2022-10-12-cof-seul",
                    "2022-10-12-bds-plus-exte",
                    "2022-10-12-del",
                ],
                ("2022-10-12-del_1", ""),
                "",
            ),
        ]
        for t in tests:
            with self.subTest(args=t):
                self.assertEqual(ensure_unique(t.string, t.not_in, t.suffix), t.result)

    def test_get_permission(self):
        for perm in [
            CREATE_GALLERY_PERMISSION,
            DELETE_GALLERY_PERMISSION,
            DELETE_FOLDER_PERMISSION,
            CHANGE_FOLDER_PERMISSION,
            CREATE_FOLDER_PERMISSION,
        ]:
            get_permission(perm)
