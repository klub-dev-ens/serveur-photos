from os import listdir
from pathlib import Path
from typing import NamedTuple

from django.contrib.auth.models import Group, User
from django.core.files.uploadedfile import UploadedFile
from django.utils.timezone import now

from ..graphics import create_photo
from ..models import Folder, Gallery, GalleryPassword, Photo


def create_user(
    username: str,
    email: str,
    password: str,
    superuser: bool = False,
    groups: list[int] = [],
) -> User:
    """Create a new user with given groups"""
    user = User.objects.create_user(
        username, email, password, is_superuser=superuser, is_staff=superuser
    )
    for group in groups:
        user.groups.add(group)
    user.save()
    return user


def create_folder(parent: str | Folder | None, name: str, slug: str) -> Folder:
    """Create a new folder"""
    if isinstance(parent, str):
        parent = Folder.objects.get(name=parent)
    folder = Folder(parent=parent, name=name, slug=slug)
    folder.save()
    return folder


def create_gallery(parent: str | Folder | None, name: str, slug: str) -> Gallery:
    if isinstance(parent, str):
        parent = Folder.objects.get(name=parent)
    gallery = Gallery(
        name=name,
        slug=slug,
        date_created=now(),
        parent=parent,
        preview=None,
        public=False,
        hidden=False,
    )
    gallery.make_path()
    gallery.save()
    return gallery


def import_from_folder(gallery: Gallery, folder: Path) -> tuple[list[Photo], list[str]]:
    """
    Adds all photos located in a filesystem folder
    (ignoring subfolders) to the given gallery
    return (added photo, files that couldn't be added)
    """
    contents = listdir(folder)
    photos = []
    errors = []
    for file in contents:
        path = folder / file
        if path.is_dir():
            continue
        with open(path, "rb") as f:
            photo = create_photo(UploadedFile(f), gallery, file)
            if photo is not None:
                photos.append(photo)
            else:
                errors.append(file)
    return photos, errors


class Perms(NamedTuple):
    """Permission to access a view based on user/keys"""

    no_auth: bool = False
    users: set[str] = set()
    keys: set[str] = set()
    instance: str = ""


def bool2status(allowed: bool, user: User | None) -> int:
    """status code returned when trying to access a gallery"""
    return 200 if allowed else (404 if user is not None else 302)


def bool2status_redirect(allowed: bool, user: User | None) -> int:
    return 302 if allowed else (404 if user is not None else 302)


def bool2status_photo(allowed: bool, _user: User | None) -> int:
    """status code returned when trying to access a photo"""
    return 200 if allowed else 404


def pk(obj: Gallery | Folder | None) -> str:
    """Value of the priary key field for POST data"""
    return "" if obj is None else str(obj.pk)


class GalleryPermissionBackup(NamedTuple):
    users_view: tuple[User, ...]
    users_upload: tuple[User, ...]
    users_delete: tuple[User, ...]
    users_edit: tuple[User, ...]
    groups_view: tuple[Group, ...]
    groups_upload: tuple[Group, ...]
    groups_delete: tuple[Group, ...]
    groups_edit: tuple[Group, ...]
    passwords: set[GalleryPassword]

    @staticmethod
    def save(gallery: Gallery) -> "GalleryPermissionBackup":
        """Initialize self with permissions from a gallery"""
        return GalleryPermissionBackup(
            users_view=tuple(gallery.can_view_users.all()),
            users_upload=tuple(gallery.can_upload_users.all()),
            users_delete=tuple(gallery.can_delete_users.all()),
            users_edit=tuple(gallery.can_edit_users.all()),
            groups_view=tuple(gallery.can_view_groups.all()),
            groups_upload=tuple(gallery.can_upload_groups.all()),
            groups_delete=tuple(gallery.can_delete_groups.all()),
            groups_edit=tuple(gallery.can_edit_groups.all()),
            passwords=set(gallery.gallerypassword_set.all()),
        )

    def restore(self, gallery: Gallery):
        """Clear gallery permissions and set them to those specified by self"""
        gallery.clear_permissions()
        gallery.can_view_users.add(*self.users_view)
        gallery.can_upload_users.add(*self.users_upload)
        gallery.can_delete_users.add(*self.users_delete)
        gallery.can_edit_users.add(*self.users_edit)
        gallery.can_view_groups.add(*self.groups_view)
        gallery.can_upload_groups.add(*self.groups_upload)
        gallery.can_delete_groups.add(*self.groups_delete)
        gallery.can_edit_groups.add(*self.groups_edit)
        GalleryPassword.objects.filter(gallery=gallery).delete()
        for pwd in self.passwords:
            pwd.save()
