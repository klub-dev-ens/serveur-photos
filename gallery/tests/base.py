from shutil import rmtree
from typing import Callable

from django.conf import settings
from django.contrib.auth.models import User
from django.http.response import HttpResponseBase
from django.test import TestCase

from ..management.commands.make_example_db import PASSWORD, Command
from ..models import GalleryPassword
from ..utils import get_permission
from .utils import Perms, create_user


class ViewTstBase(TestCase):
    cmd: Command
    keys: list[str]
    users: list[str] = ["exte", "cof", "bds", "admin", "clipper"]

    @classmethod
    def make_user_with_perm(cls, name: str, repr: str | None):
        """Create a user with given permission"""
        user = create_user(name, "oh_un_email@mail.fr", PASSWORD)
        if repr is not None:
            user.user_permissions.add(get_permission(repr))
        user.save()
        cls.users.append(name)
        return user

    photo_backup = settings.BASE_DIR / "photo_for_tst"

    @classmethod
    def setUpClass(cls) -> None:
        """Move current photo folders and initialize DB"""
        super().setUpClass()

        # move photo folders
        if settings.PHOTO_ROOT.exists():
            settings.PHOTO_ROOT.rename(cls.photo_backup)

        # init DB
        cls.cmd = Command()
        cls.cmd.verbose = False
        cls.cmd.handle()
        # get keys
        cls.keys = [x.key for x in GalleryPassword.objects.all()]
        cls.keys.append("not-a-key")

    @classmethod
    def tearDownClass(cls) -> None:
        rmtree(settings.PHOTO_ROOT)
        if cls.photo_backup.exists():
            cls.photo_backup.rename(settings.PHOTO_ROOT)
        return super().tearDownClass()

    def iter_users(
        self, perm: Perms, test_func: Callable[[bool, set[str], User | None], None]
    ):
        """Run test_func with different users and relevant permissions"""
        with self.subTest(user="no user"):
            test_func(perm.no_auth, perm.keys, None)
        for user in self.users:
            with self.subTest(user=user):
                self.assertTrue(self.client.login(username=user, password=PASSWORD))
                user_obj = User.objects.get(username=user)
                test_func(user in perm.users, perm.keys, user_obj)
                self.client.logout()

    def assertErrorMsg(self, response: HttpResponseBase) -> None:
        self.assertContains(response, '<ul class="errorlist')
