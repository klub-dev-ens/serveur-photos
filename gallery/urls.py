from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import TemplateView

from .views.folder import (
    FolderClearPreview,
    FolderCreate,
    FolderDelete,
    FolderEdit,
    FolderSetPreview,
    FolderView,
    HomeView,
)
from .views.gallery import (
    GalleryClearPreview,
    GalleryCreate,
    GalleryDelete,
    GalleryEdit,
    GalleryManage,
    GalleryPermission,
    GallerySetPreview,
    GalleryView,
    GalleryZip,
)
from .views.misc import LogoutView
from .views.photo import (
    PhotoDelete,
    PhotoDeletePermanent,
    PhotoUpload,
    PhotoView,
    ThumbsView,
)

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("dossier<path:path>", FolderView.as_view(), name="folder"),
    path("galerie<path:path>", GalleryView.as_view(), name="gallery"),
    path("modifier/dossier<path:path>", FolderEdit.as_view(), name="folder_edit"),
    path("creer/dossier<path:path>", FolderCreate.as_view(), name="folder_new"),
    path("zip<path:path>", GalleryZip.as_view(), name="zip"),
    path(
        "modifier/galerie<path:path>",
        GalleryEdit.as_view(),
        name="gallery_edit",
    ),
    path("supprimer/dossier<path:path>", FolderDelete.as_view(), name="folder_delete"),
    path(
        "supprimer/galerie<path:path>", GalleryDelete.as_view(), name="gallery_delete"
    ),
    path("creer/galerie<path:path>", GalleryCreate.as_view(), name="gallery_new"),
    path("permissions<path:path>", GalleryPermission.as_view(), name="gallery_perms"),
    path("ajouter-photo<path:path>", PhotoUpload.as_view(), name="photo_upload"),
    path("gerer<path:path>", GalleryManage.as_view(), name="gallery_manage"),
    path(
        "vignette/galerie<path:path>/<str:filename>",
        GallerySetPreview.as_view(),
        name="gallery_preview_set",
    ),
    path(
        "vignette/dossier<path:path>/<int:gallery>/<str:filename>",
        FolderSetPreview.as_view(),
        name="folder_preview_set",
    ),
    path(
        "suppr_vignette/dossier<path:path>",
        FolderClearPreview.as_view(),
        name="folder_preview_del",
    ),
    path(
        "suppr_vignette/galerie2<path:path>",
        GalleryClearPreview.as_view(),
        name="gallery_preview_del",
    ),
    path(
        "suppr_vignette/galerie<path:path>",
        GalleryClearPreview.as_view(pattern_name="gallery_manage"),
        name="gallery_preview_del2",
    ),
    path("authens/logout", LogoutView.as_view(), name="logout"),
    # These check for authentification and return redirect to file (or 404)
    # Using X-Sendfile. In Debug mode, return redirect to PHOTO_URL
    path("photo/<int:gallery>/<str:name>", PhotoView.as_view(), name="photo"),
    path(
        "thumbnail/<int:gallery>/<str:name>",
        ThumbsView.as_view(),
        name="thumbnail",
    ),
    path(
        "supprimer/<int:gallery>/<str:name>",
        PhotoDelete.as_view(),
        name="delete_photo",
    ),
    path(
        "supprimer-definitivement/<int:gallery>/<str:name>",
        PhotoDeletePermanent.as_view(),
        name="delete_photo_permanent",
    ),
    path("robots.txt", TemplateView.as_view(template_name="robots.txt"), name="robots"),
    path(
        "sitemap.xml", TemplateView.as_view(template_name="sitemap.xml"), name="sitemap"
    ),
]

if settings.DEBUG:
    urlpatterns += static(settings.DEBUG_PHOTO_URL, document_root=settings.PHOTO_ROOT)
