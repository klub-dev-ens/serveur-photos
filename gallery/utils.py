from pathlib import Path
from re import match
from typing import Iterable, NoReturn

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from config.constants import (
    FOLDER_PATH_SEPARATOR,
    GROUP_BDS,
    GROUP_CLIPPER,
    GROUP_COF,
    GROUP_TOUS,
)


def assert_never(value: NoReturn) -> NoReturn:
    """Use to assert unreachable states
    Detected by mypy
    Raise assertion failure at runtime"""
    assert False, f"Unhandled value: {value} ({type(value).__name__})"


def get_group_by_name(name: str) -> Group:
    """Return or create a group by the given name"""
    group, _created = Group.objects.get_or_create(name=name)
    return group


def get_clipper_group() -> Group:
    """Returns the special clipper Group containing all clipper users"""
    return get_group_by_name(GROUP_CLIPPER)


def get_cof_group() -> Group:
    """Returns the special COF Group"""
    return get_group_by_name(GROUP_COF)


def get_bds_group() -> Group:
    """Returns the special BDS Group"""
    return get_group_by_name(GROUP_BDS)


def get_tous_group() -> Group:
    """Returns the special Tous Group"""
    return get_group_by_name(GROUP_TOUS)


def normalize_path(path: str) -> list[str]:
    """Split a path into its components
    remove leading/trailing slashes"""
    components = path.split(FOLDER_PATH_SEPARATOR)
    # Remove leading/trailing slashes
    if components[0] == "":
        del components[0]
    if components[-1] == "":
        del components[-1]
    return components


def pretty_size(value: int) -> str:
    """Human readable file sizes: pretty_size(2654312) -> '2.6 Mio'"""
    base = 1024
    if value < base:
        return f"{value} o"
    size = float(value) / float(base)
    for unit in ["Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(size) < base:
            return f"{size:3.1f} {unit}o"
        size /= float(base)
    return f"{size:.1f} Yio"


def sorted_groups() -> list[Group]:
    """liste des groupes dans un ordre particulier:
    - les groupes spéciaux Tous, Clipper, COF et BDS en premier
    - les autres groupes par order alphabétique"""
    tous = get_tous_group()
    clipper = get_clipper_group()
    cof = get_cof_group()
    bds = get_bds_group()
    special_groups = [tous, clipper, cof, bds]
    return special_groups + [
        group for group in Group.objects.exclude(id__in=(x.pk for x in special_groups))
    ]


def ensure_unique(
    filename: str, not_in: Iterable[str], new_suffix=".jpg"
) -> tuple[str, str]:
    """Create a new unique name for the file (not in given list)
    In case of duplicate, adds a unique number IMG_022.png -> IMG_022_1.png
    Ignores any leading folders (both in filename and not_in)

    Returns a pair (filename, extension)
    """
    not_in_normalized = [Path(path).name for path in not_in]
    path = Path(filename)
    name = path.name
    suffix = ""
    if "." in name:
        split = name.split(".")
        suffix = "." + split.pop()  # remove old suffix
        newname = ".".join(split)
        if newname != "":
            # Don't erase the name of hidden files ".example"
            name = newname
        else:
            suffix = ""
    offset = 0
    append = ""
    while name + append + new_suffix in not_in_normalized:
        offset += 1
        append = "_" + str(offset)
    return name + append, suffix


def get_permission(repr: str) -> Permission:
    """Get permission from a string like 'gallery.gallery_create'
    raise ValueError or ContentType.DoesNotExist or Permission.DoesNotExist if invalid repr
    """
    matc = match(r"(.*)\.(add|delete|change|view)_(.*)", repr)
    if matc is None:
        raise ValueError(
            "Invalid permission string '{}', expected format 'app.xxx_model' "
            "where xxx is 'add', 'delete', 'change', or 'view'".format(repr)
        )
    app = matc[1]
    model = matc[3]
    perm = matc[2]
    ct = ContentType.objects.get_by_natural_key(app_label=app, model=model)
    codename = "{}_{}".format(perm, model)
    return Permission.objects.get(content_type=ct, codename=codename)
