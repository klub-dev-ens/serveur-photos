// @ts-check
/* exported slugify_watcher */

/**
 * Slugify a string, force lower case if lower is true
 *
 * @param {string} str
 * @param {boolean} lower
 * @returns {string}
 */
function slugify(str, lower) {
	// remove leading/trailing spaces
	str = str.replace(/^\s+|\s+$/g, "");
	// Make the string lowercase
	if (lower) {
		str = str.toLowerCase();
	}
	// Remove accents, swap ñ for n, etc
	const from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/,:;";
	const to = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa-----";
	for (let i = 0; i < from.length; i++) {
		str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
	}
	// Collapse whitespace and replace by -
	str = str.replace(/\s+/g, "-");
	// Remove invalid chars
	str = str.replace(/[^a-zA-Z0-9_-]/g, "");
	// Collapse dashes
	str = str.replace(/-+/g, "-");
	return str;
}

/**
 * Autocomplete slug_field with slugified name_field value
 * Stops if slug_field is changed directly, but keeps converting
 * characters inputed in slug field to slug correct characters
 *
 * @param {HTMLInputElement} name_field
 * @param {HTMLInputElement} slug_field
 */
function slugify_watcher(name_field, slug_field) {
	let changed = false;

	name_field.addEventListener("input", function (_event) {
		if (!changed) {
			slug_field.value = slugify(name_field.value, true);
		}
	});

	slug_field.addEventListener("input", function (_event) {
		changed = true;
		slug_field.value = slugify(slug_field.value, false);
	});
}
