// @ts-check
/* exported autocomplete */

const autocomplete_list = "autocomplete-list";
const autocomplete_items = "autocomplete-items";
const autocomplete_active = "autocomplete-active";

/**
 * @param {HTMLInputElement} text_field text field element
 * @param {string[]} possible_values list of possible values
 */
function autocomplete(text_field, possible_values) {

	let currentFocus = 0;

	/**
	 * Function called whenever texted is typed in the input field
	 *
	 * @param {Event} _e
	 */
	function on_input(_e) {
		if (text_field.parentNode === null) { return; } // ensure we have a parent
		// close any already open lists of autocompleted values
		closeAllLists(null);

		const val = text_field.value; // value used for autocompletion
		if (!val) { return; }

		currentFocus = -1;
		// create a DIV element that will contain the items (values):
		const dropdown = document.createElement("DIV");
		dropdown.setAttribute("id", text_field.id + autocomplete_list);
		dropdown.setAttribute("class", autocomplete_items);
		/*append the DIV element as a child of the autocomplete container:*/
		text_field.parentNode.appendChild(dropdown);

		possible_values.forEach((value) => {
			if (value.substring(0, val.length).toLowerCase() === val.toLowerCase()) {
				const dropdown_item = document.createElement("div");
				dropdown_item.innerHTML = "<strong>" + value.substring(0, val.length) + "</strong>";
				dropdown_item.innerHTML += value.substring(val.length);
				// insert a input field that will hold the current array item's value
				dropdown_item.innerHTML += "<input type='hidden' value='" + value + "'>";
				// execute a function when someone clicks on the item value (DIV element)
				dropdown_item.addEventListener("click", (_e) => {
					text_field.value = dropdown_item.getElementsByTagName("input")[0].value;
					closeAllLists(null);
				});
				dropdown.appendChild(dropdown_item);
			}
		});
	}

	/**
	 * Function called to handle arrow navigation in the dropdown list
	 * @param {KeyboardEvent} event
	 */
	function on_keypress(event) {
		const dropdown = document.getElementById(text_field.id + autocomplete_list);
		if (dropdown === null) { return; }
		const dropdown_items = Array.from(dropdown.getElementsByTagName("div"));
		switch (event.key) {
			case "ArrowDown":
				++currentFocus;
				addActive(dropdown_items);
				break;
			case "ArrowUp":
				--currentFocus;
				addActive(dropdown_items);
				break;
			case "Enter":
				event.preventDefault();
				if (currentFocus > -1) {
					/*and simulate a click on the "active" item:*/
					if (dropdown_items) { dropdown_items[currentFocus].click(); }
				}
		}
	}

	/**
	 * Close all autocomplete list in the document
	 * save those of the event target
	 * @param {EventTarget | null} element
	 */
	function closeAllLists(element) {
		const items = document.getElementsByClassName(autocomplete_items);
		for (const item of items) {
			if (element !== item && element !== text_field && item.parentNode) {
				item.parentNode.removeChild(item);
			}
		}
	}

	/**
	 * Function that changes the active element in th autocomplete list
	 * @param {HTMLElement[]} items
	 */
	function addActive(items) {
		/*a function to classify an item as "active":*/
		if (!items) { return; }
		// start by removing the "active" class on all items
		items.forEach((element) => { element.classList.remove(autocomplete_active); });
		if (currentFocus >= items.length) { currentFocus = 0; }
		if (currentFocus < 0) { currentFocus = (items.length - 1); }
		// add class "autocomplete-active"
		items[currentFocus].classList.add(autocomplete_active);
	}

	text_field.addEventListener("input", on_input);
	text_field.addEventListener("keydown", on_keypress);
	document.addEventListener("click", function (event) {
		closeAllLists(event.target);
	});
}
