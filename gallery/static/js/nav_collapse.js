// @ts-check
// Simple script to collapse the navbar on large screens

const nav_fancy = /** @type {HTMLElement} */(document.getElementById("nav_fancy"));
const nav_basic = /** @type {HTMLElement} */(document.getElementById("nav_basic"));
const home_txt = /** @type {HTMLElement} */(document.getElementById("home_txt"));

const normal_height = 48; // with no line returns, the navbar is 48px high
const compact_class = "smaller"; // class to add for smaller display

/** @returns {{width:number, height:number}};	*/
function get_nav_size() {
	if (nav_fancy) {
		return { width: nav_fancy.offsetWidth, height: nav_fancy.offsetHeight };
	}
	return { width: 0, height: 0 };
}

/** @returns {{width:number, height:number}};	*/
function get_screen_size() {
	return {
		width: document.documentElement.clientWidth,
		height: document.documentElement.clientHeight
	};
}

/**
 * @param {{width:number, height:number}} screen
 * @param {{width:number, height:number}} nav
 * @returns {boolean}
 */
function is_too_wide(screen, nav) {
	return nav.height > normal_height || nav.width > screen.width;
}

function update_navbar_display() {
	const screen = get_screen_size();
	// restore display to full
	nav_basic.style.display = "none";
	nav_fancy.style.display = "flex";
	nav_fancy.classList.remove(compact_class);
	home_txt.style.display = "inline";

	let nav = get_nav_size();

	// 1rst compacting: hide " Acceuil" text on first button
	if (is_too_wide(screen, nav)) {
		home_txt.style.display = "none";
		nav = get_nav_size();
	}

	// 2nd compacting: smaller font size and arrows
	if (is_too_wide(screen, nav)) {
		nav_fancy.classList.add(compact_class);
		nav = get_nav_size();
	}

	// 3rd compacting: down to just two buttons
	if (is_too_wide(screen, nav)) {
		nav_fancy.style.display = "none";
		nav_basic.style.display = "flex";
	}
}

update_navbar_display();
document.addEventListener("DOMContentLoaded", update_navbar_display);
window.onresize = (_c, _e) => { update_navbar_display(); };
