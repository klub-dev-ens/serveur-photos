// @ts-check
/* exported remove_file */

// HTML Elements from upload.html template
const form = /** @type {HTMLFormElement} */(document.getElementById("upload_form"));
const browse =/** @type {HTMLButtonElement} */(document.getElementById("browse"));
const dropbox = /** @type {HTMLElement} */(document.getElementById("dropbox"));
const csrftoken = /** @type {HTMLInputElement} */(document.querySelector("[name=csrfmiddlewaretoken]"));
const fileinput = /** @type {HTMLInputElement} */(document.getElementById("fileupload"));

const formbox = /** @type {HTMLElement} */(document.getElementById("formbox"));
const upload_info = /** @type {HTMLElement} */(document.getElementById("upload_info"));
const end_button = /** @type {HTMLElement} */(document.getElementById("end_button"));
const progress_info = /** @type {HTMLElement} */(document.getElementById("progress_info"));
const upload_stats = /** @type {HTMLElement} */(document.getElementById("upload_stats"));
const photo_count = /** @type {HTMLElement} */(document.getElementById("photocount"));
const filelist = /** @type {HTMLElement} */(document.getElementById("filelist"));
const submit_button = /** @type {HTMLButtonElement} */(document.getElementById("submit"));

let upload_in_progress = false;
let upload_index = 0;
let uploaded_data = 0;
let average_speed = "Calcul...";
let total_data = "...";
let upload_start = 0;

const WAITING = 1;
const IN_PROGRESS = 2;
const DONE = 3;
const ERROR = 4;
/** @typedef {WAITING | IN_PROGRESS | DONE | ERROR} status */

/** @type {{file:File, status:status}[]} */
const files_to_upload = [];

/**
 * @param {number} size size (in bytes) to pretty print
 * @returns {string}
 * */
function pretty_size(size) {
	const base = 1024;
	if (size < base) { return `${size.toFixed(1)} o`; }
	size = size / base;
	for (const unit of ["Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]) {
		if (size < base) { return `${size.toFixed(1)} ${unit}o`; }
		size /= base;
	}
	return `${size.toFixed(1)} Yio`;
}

/**
 * @param {number} time (in milliseconds)
 * @returns {string}
 * */
function pretty_time(time) {
	time /= 1000;
	let repr = `${(time % 60).toFixed(1)} s`;
	if (time > 60) {
		time /= 60;
		repr = `${time % 60} min ${repr}`;
		if (time > 60) {
			time /= 60;
			repr = `${time % 60} h ${repr}`;
		}
	}
	return repr;
}

/**
 * Shorthand for colored fontawesome icons
 * @param {string} icon
 * @param {string} color
 * @returns {string} the html element
 */
function icon(icon, color) {
	return `<i class="fa-solid fa-${icon}" style="color: ${color};"></i>`;
}

/**
 * Pretty print a state
 * @param {status} state
 * @returns {string}
 */
function pretty_state(state) {
	switch (state) {
		case WAITING: return `${icon("hourglass", "#D6D5A8")} En attente...`;
		case IN_PROGRESS: return `${icon("rotate", "#44f")} En cours...`;
		case DONE: return `${icon("circle-check", "#4f4")} Terminé`;
		case ERROR: return `${icon("triangle-exclamation", "#f44")} Erreur`;
	}
}

/** @param {number} index file element to remove */
function remove_file(index) {
	files_to_upload.splice(index, 1);
	update_files_list();
}

/** Update the file list table display */
function update_files_list() {
	photo_count.innerHTML = `${files_to_upload.length}`;

	let table = `<tr>
		<th>Nom</th>
		<th>Taille</th>
		<th>${upload_in_progress ? "État" : ""}</th>
	</tr>`;
	for (let i = 0; i < files_to_upload.length; ++i) {
		const obj = files_to_upload[i];
		const file = obj.file;
		const state = upload_in_progress
			? pretty_state(obj.status)
			: `<a onclick="remove_file(${i})"><i class="fa-regular fa-circle-xmark"></i></>`;
		table += `<tr${i === files_to_upload.length - 1 ? " class=\"sep\"" : ""}>
			<td>${file.name}</td>
			<td>${pretty_size(file.size)}</td>
			<td>${state}</td>
		</tr>`;
	}
	filelist.innerHTML = table;

	if (files_to_upload.length === 0) {
		submit_button.classList.add("disabled");
		submit_button.title = "Aucune photo sélectionnée";
	}
	else {
		submit_button.classList.remove("disabled");
		submit_button.title = "";
	}
	upload_stats.innerHTML = `<tr>
			<th>Photos transférées :</th>
			<td>${upload_index} / ${files_to_upload.length} (${pretty_size(uploaded_data)} / ${total_data})</td>
		</tr>
		<tr>
			<th>Vitesse moyenne :</th>
			<td>${average_speed}</td>
		</tr>
		<tr>
			<th>Temps écoulé :</th>
			<td>${pretty_time(Date.now() - upload_start)}</td>
		</tr>`;
}

// ====================================
// Dropbox behavior
// ====================================

/** @param {DragEvent} evt */
function on_drag(evt) {
	evt.preventDefault();
	evt.stopPropagation();
}

/** @param {DragEvent} evt */
function on_drop(evt) {
	evt.preventDefault();
	evt.stopPropagation();
	if (evt.dataTransfer !== null) {
		console.log(evt.dataTransfer.files);
		for (const file of evt.dataTransfer.files) {
			files_to_upload.push({ file: file, status: WAITING });
		}
	}
	update_files_list();
}

dropbox.addEventListener("dragover", on_drag);
dropbox.addEventListener("dragenter", on_drag);
dropbox.addEventListener("dragleave", on_drag);
dropbox.addEventListener("drop", on_drop);

// ====================================
// Browse button
// ====================================

/** @param {MouseEvent} evt */
function browse_click(evt) {
	evt.preventDefault();
	fileinput.click();
}

/** @param {Event} _evt */
function on_file_input_change(_evt) {
	if (fileinput.files !== null) {
		for (const file of fileinput.files) {
			files_to_upload.push({ file: file, status: WAITING });
		}
	}
	update_files_list();
}

browse.addEventListener("click", browse_click);
fileinput.addEventListener("change", on_file_input_change);

// =======================================
// Async file upload
// =======================================

form.addEventListener("submit", submit);

/**
 * @param {File} file
 * @returns {Promise<boolean>} true if upload succeeds
 * */
async function send_file(file) {
	const data = new FormData();
	data.append("file", file);
	data.append("filename", file.name);
	const response = await fetch(
		form.target,
		{
			method: form.method,
			headers: {
				"X-CSRFToken": csrftoken.value
			},
			body: data,
		}
	);
	if (response.status !== 200) { return false; }
	const json = await response.json();
	return json.status === "ok";
}

/** @param {SubmitEvent} event */
async function submit(event) {
	event.preventDefault();

	if (files_to_upload.length === 0) {
		window.alert("Aucun fichier à téléverser.");
		return;
	}
	total_data = pretty_size(files_to_upload.reduce(
		(t, x) => { return x.file.size + t; },
		0)
	);
	formbox.style.display = "none";
	form.style.display = "none";
	upload_info.style.display = "block";
	upload_in_progress = true;

	upload_start = Date.now();
	let nb_errors = 0;

	for (; upload_index < files_to_upload.length; ++upload_index) {
		const file = files_to_upload[upload_index].file;
		files_to_upload[upload_index].status = IN_PROGRESS;
		update_files_list();
		const success = await send_file(file);
		files_to_upload[upload_index].status = success ? DONE : ERROR;
		if (success === false) { nb_errors++; }
		uploaded_data += file.size;
		average_speed = pretty_size(uploaded_data * 1000 / (Date.now() - upload_start)) + "/s";
	}
	update_files_list();
	let error_msg = nb_errors > 0
		? "Les erreurs peuvent être du à un problème de connexion ou de format. Si elles persistent, essayer de convertir les photos en .jpg.<br>"
		: "";
	progress_info.innerHTML = `
		Téléversement terminé.<br>
		${files_to_upload.length - nb_errors} / ${files_to_upload.length} photos téléversées avec succès.<br>
		${error_msg}
	`;
	end_button.style.display = "block";
	upload_in_progress = false;
}

window.onbeforeunload = function () {
	if (upload_in_progress) {
		return "Quitter cette page interrompra le téléversement. Êtes-vous sur de vouloir quitter ?";
	}
};
