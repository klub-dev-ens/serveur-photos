// @ts-check
/* exported toggle_dropdown */

const show_class = "show"; // same as in CSS
const dropdown_button_class = "dropbutton";
const dropdown_content_class = "dropdown-content";

/** @param {string} id */
function toggle_dropdown(id) {
	// When the user clicks on the button,
	// toggle between hiding and showing the dropdown content
	const elem = document.getElementById(id);
	if (elem !== null) {
		elem.classList.toggle(show_class);
	}
}

/** @param {Element} dropdown */
function hide_dropdown(dropdown) {
	// hides a dropdown menu
	dropdown.classList.remove(show_class);
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
	if (!event.target.matches("." + dropdown_button_class)) {
		const dropdowns = document.getElementsByClassName(dropdown_content_class);
		for (const dropdown of dropdowns) {
			hide_dropdown(dropdown);
		}
	}
};
