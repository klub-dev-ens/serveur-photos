from django.contrib import admin

from config.constants import TITLE

from .models import Folder, Gallery, GalleryPassword, Photo

# Titre de la vue (tag <h1>)
admin.site.site_header = f"Administration {TITLE}"
# Tag html <title>
admin.site.site_title = f"Admin {TITLE}"


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    """Options d'affichage du modèle photo dans la vue django admin"""

    list_display = ("filename", "gallery", "date_uploaded", "pretty_size", "visible")
    list_filter = ("gallery", "visible")
    ordering = ("gallery", "filename")


@admin.register(Folder)
class FolderAdmin(admin.ModelAdmin):
    """Options d'affichage du modèle Folder dans la vue django admin"""


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    """Options d'affichage pour un modèle Gallery dans django admin"""

    list_display = ("__str__", "date_created", "path", "hidden", "pretty_size")
    filter_horizontal = (
        "can_view_users",
        "can_view_groups",
        "can_upload_users",
        "can_upload_groups",
        "can_delete_users",
        "can_delete_groups",
        "can_edit_users",
        "can_edit_groups",
    )


@admin.register(GalleryPassword)
class GalleryPasswordAdmin(admin.ModelAdmin):
    """Options d'affichage pour un modèle GalleryPassword dans django admin"""

    list_display = (
        "gallery",
        "key",
        "expires",
    )
    ordering = ("gallery", "expires")
    list_filter = ("gallery",)
