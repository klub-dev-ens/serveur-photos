from django import template

from ..models import AnyUser, Folder, Photo

register = template.Library()


@register.simple_tag
def folder_dynamic_preview(
    folder: Folder, user: AnyUser, key: str | None
) -> Photo | None:
    """custom tag to pass user and key to a folder method"""
    return folder.dynamic_preview(user, key)
