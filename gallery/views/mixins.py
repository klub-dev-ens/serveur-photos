from enum import Enum
from typing import Any

from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpRequest, HttpResponseRedirect
from django.urls import reverse
from django.views.generic.base import ContextMixin

from config.constants import KEY_QUERY_PARAM_NAME

from ..models import Folder, Gallery, encode_path
from ..utils import assert_never, normalize_path


def full_path(folder: Gallery | Folder | None) -> list[Folder | Gallery]:
    """
    Returns all folders leading to and including argument:
    Eg full_path(Folder(/a/b/c/)) is [Folder(/a/), Folder(/a/b/), Folder(/a/b/c/)]
    Eg full_path(Gallery(/a/b/c)) is [Folder(/a/), Folder(/a/b/), Gallery(/a/b/c)]
    """
    if folder is None:
        return []
    path = full_path(folder.parent)
    path.append(folder)
    return path


def reverse_path(view: str, obj: Gallery | Folder | None) -> str:
    """Reverse a view with a path kwarg"""
    path = str(obj) if obj else "/"
    return reverse(view, kwargs={"path": path})


class Permission(Enum):
    VIEW = 0
    UPLOAD_PHOTO = 1
    DELETE_PHOTO = 2
    EDIT = 3
    CREATE = 4
    DELETE = 5


CREATE_GALLERY_PERMISSION = "gallery.add_gallery"
DELETE_GALLERY_PERMISSION = "gallery.delete_gallery"

CREATE_FOLDER_PERMISSION = "gallery.add_folder"
DELETE_FOLDER_PERMISSION = "gallery.delete_folder"
CHANGE_FOLDER_PERMISSION = "gallery.change_folder"


class CanAccessMixin(UserPassesTestMixin, ContextMixin):
    """
    Mixin used for multiple purposes
    - find key if any and set self.key
    - find folder/gallery referenced in url path and set it in self.obj
    - check if user can access the object with the required permission

    There are two different causes for error (404 not found or 403 permission denied)
    But we only use one http return code to avoid leaking information about folder structure.
    The behavior is therefore as follows:
    - user not authenticated and 404 or 403 => redirect to login with current url as next
    - user authenticated and 404 or 403 => 404
    """

    # These attributes must be set by subclasses
    type: type[Gallery] | type[Folder]  # Gallery or Folder
    permission: Permission

    # Attributes set by this mixin
    key: str | None  # The passkey found in URL params
    path: str | None  # The path passed in URL
    obj: Gallery | Folder | None  # The object the path points to

    # Set by django
    request: HttpRequest

    def set_obj(self) -> bool:
        """Decodes path and sets the obj attribute
        Returns True if success, False if failure"""
        if self.path is not None:
            path = normalize_path(self.path)
            try:
                self.obj = self.type.from_path(path)
            except ValueError:
                return False
        return True

    def can_access(self) -> bool:
        """Returns True if the given user can access this view
        (checks self.permission)"""
        raise NotImplementedError()

    def dispatch(self, request, path: str | None = None, *args, **kwargs):
        """We need to override dispatch to easily access path and key in test_func()"""
        # self.request = request
        self.path = path
        self.key = request.GET.get(KEY_QUERY_PARAM_NAME)
        return super().dispatch(request, *args, **kwargs)

    def test_func(self) -> bool:
        """Checks that the path exists and set self.obj to its object"""
        return self.set_obj() and self.can_access()

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        """
        if key exists, then adds key to context formatted so that it can be appended to urls:
        {{ key }} is ?cle=<key>, can be used as {% url "something" %}{{ key }}
        """
        context = super().get_context_data(**kwargs)
        context["true_key"] = self.key
        context["key"] = f"?{KEY_QUERY_PARAM_NAME}={self.key}" if self.key else ""
        path = full_path(self.obj)
        if path and isinstance(path[-1], Gallery):
            context["gallery"] = path.pop()
        if self.obj is not None:
            context["parent"] = self.obj.parent
        context["path"] = path
        return context

    def handle_no_permission(self) -> HttpResponseRedirect:
        """Redirect to login if not authenticated
        Raise 404 otherwise"""
        try:
            # Django's UserPassesTestMixin automatically redirects to login
            # if not authenticated
            return super().handle_no_permission()
        except PermissionDenied:
            # We return 404 if django tries to raise 403
            raise Http404()

    def reverse(self, pattern_name: str, *args, **kwargs) -> str:
        kwargs["path"] = encode_path(self.obj)
        return reverse(pattern_name, args=args, kwargs=kwargs)


class GalleryAccessMixin(CanAccessMixin):
    """Version de CanAccessMixin spécifique aux galleries"""

    type = Gallery

    obj: Gallery

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context["can_delete_gallery"] = self.obj.can_delete(
            user
        ) and self.request.user.has_perm(DELETE_GALLERY_PERMISSION)
        context["can_delete"] = self.obj.can_delete(user)
        context["can_edit"] = self.obj.can_edit(user)
        context["can_upload"] = self.obj.can_upload(user)
        context["photos"] = self.obj.photo_set.filter(visible=True)
        return context

    def can_access(self) -> bool:
        if self.permission is Permission.VIEW:
            return self.obj.can_view(self.request.user, self.key)
        if self.permission is Permission.EDIT:
            return self.obj.can_edit(self.request.user)
        if self.permission is Permission.UPLOAD_PHOTO:
            return self.obj.can_upload(self.request.user)
        if self.permission is Permission.DELETE_PHOTO:
            return self.obj.can_delete(self.request.user)
        if self.permission is Permission.CREATE:
            return self.request.user.has_perm(CREATE_GALLERY_PERMISSION)
        if self.permission is Permission.DELETE:
            return self.obj.can_edit(self.request.user) and self.request.user.has_perm(
                DELETE_GALLERY_PERMISSION
            )
        assert_never(self.permission)


class FolderAccessMixin(CanAccessMixin):
    """Version de CanAccessMixin spécifique aux dossiers"""

    type = Folder
    obj: Folder | None

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context["strpath"] = str(self.obj) if self.obj is not None else "/"
        context["new_folder"] = user.has_perm(CREATE_FOLDER_PERMISSION)
        context["new_gallery"] = user.has_perm(CREATE_GALLERY_PERMISSION)
        context["edit"] = user.has_perm(CHANGE_FOLDER_PERMISSION)
        context["delete"] = user.has_perm(DELETE_FOLDER_PERMISSION)
        return context

    def can_access(self) -> bool:
        if self.permission == Permission.VIEW:
            # Can always view root
            return self.obj is None or self.obj.can_view(self.request.user, self.key)
        if self.permission is Permission.CREATE:
            return self.request.user.has_perm(CREATE_FOLDER_PERMISSION)
        if self.obj is None:
            return False  # can't edit/delete the root
        if self.permission is Permission.DELETE:
            return self.request.user.has_perm(DELETE_FOLDER_PERMISSION)
        if self.permission is Permission.EDIT:
            return self.request.user.has_perm(CHANGE_FOLDER_PERMISSION)
        return False
