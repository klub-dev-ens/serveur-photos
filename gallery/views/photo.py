from django.conf import settings
from django.contrib import messages
from django.http import Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import FormView, View

from config.constants import KEY_QUERY_PARAM_NAME

from ..forms import PhotoUploadForm
from ..graphics import create_photo
from ..models import Photo
from .mixins import GalleryAccessMixin, Permission, reverse_path


def xsendfile_redirect(path: str) -> HttpResponse:
    """Redirect access to a static photo/thumbnail/zipfile via X-Sendfile
    In debug mode, redirects to self manages static URL"""
    if settings.DEBUG:
        return HttpResponseRedirect("/" + settings.DEBUG_PHOTO_URL + path)

    response = HttpResponse()
    response[settings.X_SENDFILE_HEADER] = str(settings.X_SENDFILE_REDIRECT_TO / path)
    return response


class PhotoView(View):
    """Check that photo exists and user can access it
    then redirect to photo using X_Sendfile in prod
    or simpler redirect in DEBUG"""

    is_thumbs = False

    def can_access(self, key: str, photo: Photo) -> bool:
        """Check permissions"""
        if photo.visible:
            return photo.gallery.can_view(self.request.user, key)
        # Hidden photo only visible to those who have delete/edit
        # permission on the gallery
        return photo.gallery.can_edit(self.request.user)

    def get(self, request, gallery: int, name: str, **kwargs) -> HttpResponse:
        """Create response to GET requests"""
        key = request.GET.get(KEY_QUERY_PARAM_NAME)
        photo = get_object_or_404(Photo, gallery=gallery, filename=name)
        if not self.can_access(key, photo):
            raise Http404()
        return self.make_response(photo)

    def make_response(self, photo: Photo) -> HttpResponse:
        """
        Create the response, assuming the user passed authentification tests
        """
        path = photo.thumbnail_path() if self.is_thumbs else photo.path()
        return xsendfile_redirect(path)


class ThumbsView(PhotoView):
    is_thumbs = True


class PhotoDelete(PhotoView):
    """Delete/restore photo
    This is not permanent deletion, the files are kept.
    The photo is just marked as non-visible in the DB"""

    pattern_name = "gallery_manage"

    def can_access(self, key: str, photo: Photo) -> bool:
        """
        Checks for delete permission before deleting
        Need edit permission to restore delete photos
        """
        if photo.visible:
            return photo.gallery.can_delete(self.request.user)
        return photo.gallery.can_edit(self.request.user)

    def make_response(self, photo: Photo) -> HttpResponse:
        photo.visible = not photo.visible
        message = "Photo supprimée avec succès"
        if photo.visible:
            message = "Photo restaurée avec succès"
        else:
            photo.remove_from_previews()
        photo.save()
        messages.success(self.request, message)
        return HttpResponseRedirect(reverse_path(self.pattern_name, photo.gallery))


class PhotoDeletePermanent(PhotoDelete):
    """Utiliser pour supprimer définitivement une photo"""

    def can_access(self, key: str, photo: Photo) -> bool:
        return photo.gallery.can_edit(self.request.user) and (not photo.visible)

    def make_response(self, photo: Photo) -> HttpResponse:
        photo.delete()
        messages.success(self.request, "Photo supprimée définitivement")
        return HttpResponseRedirect(reverse_path(self.pattern_name, photo.gallery))


class PhotoUpload(GalleryAccessMixin, FormView):
    form_class = PhotoUploadForm
    template_name = "forms/upload.html"
    permission = Permission.UPLOAD_PHOTO
    pattern_name = "gallery"

    def response(self, status) -> JsonResponse:
        return JsonResponse({"status": status})

    def form_valid(self, form: PhotoUploadForm) -> JsonResponse:
        file = self.request.FILES.get("file")
        if file is None:
            return self.response("No file uploaded")
        name: str | None = form.cleaned_data.get("filename")
        name = file.name if name is None else name
        photo = create_photo(file, self.obj, name)
        if photo is None:
            return self.response("Unrecognized photo format")
        return self.response("ok")

    def form_invalid(self, form: PhotoUploadForm) -> JsonResponse:
        return self.response("Invalid form {}".format(form.errors))
