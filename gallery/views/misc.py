from authens.views import LogoutView as AuthensLogoutView
from django.urls import reverse_lazy


class LogoutView(AuthensLogoutView):
    """Overwrite authens's logout to safely redirect afterwards"""

    next_page = reverse_lazy("home")
