from typing import Any

from django.contrib import messages
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect
from django.http.response import HttpResponseBase
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import RedirectView, TemplateView, UpdateView

from config.models import SiteConfig

from ..forms import FolderForm
from ..models import Folder, Gallery, Photo
from .mixins import FolderAccessMixin, Permission, reverse_path


class FolderView(FolderAccessMixin, TemplateView):
    """View to browse (display) the contents of a folder"""

    permission = Permission.VIEW
    template_name = "folder.html"

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        """Adds the following to the context:
        - folder: the current folder (or None for root)
        - galleries: direct subgalleries the user can view
        - subfolders: direct subfolders the user can view
        """
        user = self.request.user
        context = super().get_context_data(**kwargs)
        context["folder"] = self.obj
        context["galleries"] = list(
            gallery
            for gallery in Folder.get_galleries(self.obj)
            if gallery.can_view(user, self.key)
        )
        context["subfolders"] = list(
            folder
            for folder in Folder.get_subfolders(self.obj)
            if folder.can_view(user, self.key)
        )
        return context

    def get(self, request: HttpRequest, *args, **kwargs) -> HttpResponse:
        """Return to home screen when accessing /dossier/"""
        if not isinstance(self, HomeView) and self.obj is None:
            return HttpResponseRedirect(reverse("home"))
        return super().get(request, *args, **kwargs)


class HomeView(FolderView):
    """The home page"""

    obj = None
    template_name = "home.html"

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        config = SiteConfig.load()
        context = super().get_context_data(**kwargs)
        recent = Gallery.objects.order_by("-date_created")
        found: list[Gallery] = []
        for gallery in recent:
            if len(found) >= config.nb_recent_galleries:
                break
            if gallery.can_view(self.request.user, self.key):
                found.append(gallery)
        context["recent"] = found
        return context


class FolderEdit(FolderAccessMixin, UpdateView):
    """Vue pour modifier un dossier"""

    permission = Permission.EDIT
    form_class = FolderForm
    template_name = "forms/folder.html"
    path: str | None = None

    title = "Modifier un dossier"
    message = "Dossier {} modifié avec succès"

    object: Folder | None

    def get_object(self, _queryset=None) -> Folder | None:
        """Return the object related to self
        _queryset is just there to preserve inherited type"""
        if self.obj is None:
            raise Http404()  # Can't modify the root
        # We use this to create a copy of self.obj to preserve it
        return Folder.objects.get(pk=self.obj.pk)

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["title"] = self.title
        return context

    def get_success_url(self):
        """Redirect to folder"""
        messages.success(self.request, self.message.format(self.object))
        return reverse_path("folder", self.object)


class FolderCreate(FolderEdit):
    """Vue pour la création de nouveau dossiers"""

    permission = Permission.CREATE

    title = "Créer un dossier"
    message = "Dossier {} crée avec succès"

    initial: dict[str, Any] = {}

    def get_object(self, _queryset=None) -> Folder | None:
        """Return the object related to self
        _queryset is just there to preserve inherited type"""
        self.initial["parent"] = self.obj  # set initial to path
        return None


class FolderDelete(FolderAccessMixin, RedirectView):
    """Vue pour supprimer un dossier"""

    permission = Permission.DELETE

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        if self.obj is not None:
            if (
                Folder.get_galleries(self.obj).exists()
                or Folder.get_subfolders(self.obj).exists()
            ):
                messages.error(
                    self.request, "Impossible de supprimer un dossier non-vide"
                )
                return reverse_path("folder", self.obj)
            parent = self.obj.parent

            self.obj.delete()
            messages.success(self.request, "Dossier {} supprimé.".format(self.obj))
            if parent is None:
                return reverse("home")
            return reverse_path("folder", parent)
        raise Http404()


class FolderSetPreview(FolderAccessMixin, RedirectView):
    """View to change the preview image of a folder"""

    pattern_name: str = "folder"
    permission = Permission.EDIT

    def get_redirect_url(self, *args, **kwargs) -> str:
        return self.reverse(self.pattern_name)

    def get(
        self,
        request: HttpRequest,
        gallery: int | None = None,
        filename: str | None = None,
        *args,
        **kwargs,
    ) -> HttpResponseBase:
        photo = get_object_or_404(Photo, gallery=gallery, filename=filename)
        if (
            self.obj is not None
            and photo.visible
            and self.obj.is_ancestor_of(photo.gallery)
        ):
            self.obj.preview = photo
            self.obj.save()
            messages.success(
                request, "Vignette du dossier {} changée avec succès !".format(self.obj)
            )
        else:
            raise Http404()
        return super().get(request, *args, **kwargs)


class FolderClearPreview(FolderAccessMixin, RedirectView):
    """Vue pour retirer la vignette d'un dossier"""

    permission = Permission.EDIT

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        if self.obj is not None and self.obj.preview is not None:
            self.obj.preview = None
            self.obj.save()
            messages.success(self.request, "Vignette supprimée")
        else:
            raise Http404()
        return reverse_path("folder_edit", self.obj)
