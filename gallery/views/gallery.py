from typing import Any, Mapping

from django.contrib import messages
from django.contrib.auth.models import User
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect
from django.http.response import HttpResponseBase
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils.timezone import now
from django.views.generic import RedirectView, TemplateView, UpdateView, View

from config.constants import (
    DEFAULT_GROUP_PERMISSIONS,
    KEY_CHARS,
    KEY_LENGTH,
    KEY_VALIDITY,
)
from config.models import SiteConfig

from ..forms import (
    GalleryForm,
    GalleryPasswordFormsetType,
    GalleryPermissionForm,
    GroupFormsetType,
    UserFormsetType,
    UserPermissionFormset,
    gallery_password_formset_factory,
    group_permission_formset_factory,
)
from ..models import Folder, Gallery, GalleryPassword, Photo
from ..utils import get_group_by_name, sorted_groups
from .mixins import (
    CREATE_GALLERY_PERMISSION,
    FolderAccessMixin,
    GalleryAccessMixin,
    Permission,
    reverse_path,
)
from .photo import xsendfile_redirect


class GalleryView(GalleryAccessMixin, TemplateView):
    """View to browse (display) the contents of a gallery"""

    permission = Permission.VIEW
    template_name = "gallery.html"


class GalleryCreate(FolderAccessMixin, UpdateView):
    """Version du formulaire spécifique à la création de nouvelles galleries"""

    template_name = "forms/gallery.html"
    form_class = GalleryForm

    title = "Nouvelle galerie"

    def get_object(self, _queryset=None) -> Folder | None:
        """Return the object related to self
        _queryset is just there to preserve inherited type"""
        self.initial["parent"] = self.obj  # set initial to path
        return None

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["title"] = self.title
        context["new"] = True
        return context

    def get_success_url(self):
        """Redirect to folder"""
        messages.success(self.request, "Galerie {} créée".format(self.object))
        return reverse_path("gallery_perms", self.object)

    def can_access(self) -> bool:
        """Overwrite FolderMixin permission - we are creating a gallery"""
        return self.request.user.has_perm(CREATE_GALLERY_PERMISSION)

    def form_valid(self, form: GalleryForm) -> HttpResponse:
        """Initialize extra fields and permissions"""
        gallery = form.instance
        gallery.date_created = now()
        gallery.make_path()
        gallery.save()

        # Default group permissions
        for perms in DEFAULT_GROUP_PERMISSIONS:
            group_pk = get_group_by_name(perms.entity_name).pk
            if perms.view:
                gallery.can_view_groups.add(group_pk)
            if perms.upload:
                gallery.can_upload_groups.add(group_pk)
            if perms.delete:
                gallery.can_delete_groups.add(group_pk)
            if perms.edit:
                gallery.can_edit_groups.add(group_pk)

        # Ensure creator has all rights to the gallery
        user = self.request.user
        if (
            not gallery.can_view(user, None)
            or not gallery.can_upload(user)
            or not gallery.can_delete(user)
            or not gallery.can_edit(user)
        ):
            gallery.can_view_users.add(user.pk)
            gallery.can_upload_users.add(user.pk)
            gallery.can_edit_users.add(user.pk)
            gallery.can_delete_users.add(user.pk)

        gallery.save()
        return super().form_valid(form)


class GalleryEdit(GalleryAccessMixin, UpdateView):
    """Vue pour modifier une galerie"""

    template_name = "forms/gallery.html"
    form_class = GalleryForm
    permission = Permission.EDIT

    title = "Modifier une galerie"

    def get_object(self, _queryset=None) -> Gallery:
        """Return the object related to self
        _queryset is just there to preserve inherited type"""
        # We use this to create a copy of self.obj to preserve it
        return Gallery.objects.get(pk=self.obj.pk)

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["title"] = self.title
        return context

    def get_success_url(self):
        """Redirect to folder"""
        messages.success(
            self.request, "Galerie {} modifiée avec succès".format(self.obj)
        )
        return reverse_path("gallery_manage", self.object)


class GalleryPermission(GalleryAccessMixin, View):
    """Vue pour gèrer les permissions d'une galerie
    Assez laborieuse parce qu'elle mixe forms et formset en un gros formulaire
    - Un form simple pour les champs "public" et "masqué"
    - Un formset pour les permissions de groupes
    - Un formset pour les permissions d'utilisateurs
    - Un formset pour les liens d'accès
    """

    template_name = "forms/permissions.html"
    group_form_prefix = "form_group"
    user_form_prefix = "form_user"
    key_form_prefix = "key_form"

    permission = Permission.EDIT

    def get_form(self, post: Mapping[str, Any] | None) -> GalleryPermissionForm:
        """Génère le formulaire simple"""
        return GalleryPermissionForm(post, instance=self.obj)

    def get_group_formset(self, post: Mapping[str, Any] | None) -> GroupFormsetType:
        """Génère le formset des permission de chaque groupe"""
        gallery = self.obj
        formset = group_permission_formset_factory()
        return formset(
            post,
            initial=[
                {
                    "group": group,
                    "can_view": group in gallery.can_view_groups.all(),
                    "can_upload": group in gallery.can_upload_groups.all(),
                    "can_delete": group in gallery.can_delete_groups.all(),
                    "can_edit": group in gallery.can_edit_groups.all(),
                }
                for group in sorted_groups()
            ],
            prefix=self.group_form_prefix,
        )

    def get_user_formset(self, post: Mapping[str, Any] | None) -> UserFormsetType:
        """Génère le formulaire de permission de chaque utilisateur"""
        if post is not None:
            return UserPermissionFormset(post, prefix=self.user_form_prefix)
        gallery = self.obj
        users = (
            gallery.can_view_users.all()
            | gallery.can_upload_users.all()
            | gallery.can_delete_users.all()
            | gallery.can_edit_users.all()
        )
        return UserPermissionFormset(
            initial=[
                {
                    "user": user.username,
                    "can_view": user in gallery.can_view_users.all(),
                    "can_upload": user in gallery.can_upload_users.all(),
                    "can_delete": user in gallery.can_delete_users.all(),
                    "can_edit": user in gallery.can_edit_users.all(),
                }
                for user in users.order_by("username").distinct()
            ],
            prefix=self.user_form_prefix,
        )

    def get_key_formset(
        self, post: Mapping[str, Any] | None
    ) -> GalleryPasswordFormsetType:
        """returns initial data for the formset"""
        formset = gallery_password_formset_factory(self.obj)
        queryset = self.obj.gallerypassword_set.order_by("expires")
        if post is not None:
            return formset(post, prefix=self.key_form_prefix, queryset=queryset)
        return formset(prefix=self.key_form_prefix, queryset=queryset)

    def get_context_data(self, post=None, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context["users"] = User.objects.filter(is_active=True)
        context["form"] = self.get_form(post)
        context["form_groups"] = self.get_group_formset(post)
        context["form_users"] = self.get_user_formset(post)
        keys = self.get_key_formset(post)
        context["keys"] = keys
        context["keys_nonempty"] = len(keys) != 0
        return context

    def get(self, request, *args, **kwargs) -> HttpResponse:
        context = self.get_context_data()
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs) -> HttpResponse:
        form = self.get_form(request.POST)
        group_forms = self.get_group_formset(request.POST)
        user_forms = self.get_user_formset(request.POST)
        key_forms = self.get_key_formset(request.POST)
        new_key = request.POST.get("new_key") == "True"
        if not (
            form.is_valid()
            and group_forms.is_valid()
            and user_forms.is_valid()
            and key_forms.is_valid()
        ):
            context = self.get_context_data(request.POST)
            return render(request, self.template_name, context)
        # Data is valid
        gallery = form.save()
        # Clear all permissions, then reset them
        gallery.clear_permissions()
        for group_form in group_forms.forms:
            group_form.save_permissions(gallery)
        for user_form in user_forms.forms:
            user_form.save_permissions(gallery)
        key_forms.save()

        if new_key:
            password = self.generate_key()
            url = password.url(request.get_host())
            messages.success(request, f"Nouveau lien d'accès créé: {url}")
            pattern_name = "gallery_perms"
        else:
            pattern_name = "gallery_manage"
            messages.success(
                self.request, "Permissions de '{}' enregistrées".format(gallery)
            )
        return HttpResponseRedirect(reverse_path(pattern_name, gallery))

    def generate_key(self) -> GalleryPassword:
        """
        Create a new unique password
        """
        new_key = User.objects.make_random_password(
            length=KEY_LENGTH, allowed_chars=KEY_CHARS
        )
        if GalleryPassword.objects.filter(key=new_key).exists():
            # Avoid duplicate key
            return self.generate_key()

        password = GalleryPassword(
            key=new_key, gallery=self.obj, expires=now() + KEY_VALIDITY
        )
        password.save()
        return password


class GalleryDelete(GalleryAccessMixin, RedirectView):
    """Vue pour supprimer un dossier"""

    permission = Permission.DELETE

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        if self.obj.photo_set.exists():
            messages.error(self.request, "Impossible de supprimer une galerie non-vide")
            return reverse_path("gallery_manage", self.obj)
        parent = self.obj.parent
        self.obj.delete()
        messages.success(self.request, "Galerie {} supprimé.".format(self.obj))
        if parent is None:
            return reverse("home")
        return reverse_path("folder", parent)


class GalleryManage(GalleryAccessMixin, TemplateView):
    """
    View for managing a gallery: displays information and allows
    access to the gallery forms
    """

    template_name = "gallery_manage.html"

    def can_access(self) -> bool:
        user = self.request.user
        return self.obj.can_edit(user) or self.obj.can_delete(user)

    def get_context_data(self, *args, **kwargs) -> dict[str, Any]:
        context = super().get_context_data()
        context["photos_deleted"] = self.obj.photo_set.filter(visible=False)
        return context


class GallerySetPreview(GalleryAccessMixin, RedirectView):
    """View to change the preview image of a gallery"""

    pattern_name: str = "gallery_manage"
    permission = Permission.EDIT

    def get(
        self, request: HttpRequest, filename: str | None = None, *args, **kwargs
    ) -> HttpResponseBase:
        photo = get_object_or_404(Photo, gallery=self.obj.pk, filename=filename)
        if photo.visible:
            self.obj.preview = photo
            self.obj.save()
            messages.success(
                request, "Vignette de galerie {} changée avec succès !".format(self.obj)
            )
        else:
            raise Http404()
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs) -> str:
        return self.reverse(self.pattern_name, *args, **kwargs)


class GalleryClearPreview(GalleryAccessMixin, RedirectView):
    """Vue pour retirer la vignette d'une gallerie"""

    permission = Permission.EDIT
    pattern_name: str = "gallery_edit"

    def get_redirect_url(self, *args: Any, **kwargs: Any) -> str | None:
        if self.obj.preview is not None:
            self.obj.preview = None
            self.obj.save()
            messages.success(self.request, "Vignette supprimée")
        else:
            raise Http404()
        return reverse_path(self.pattern_name, self.obj)


class GalleryZip(GalleryAccessMixin, View):
    """Vue pour télécharger une gallerie au format ZIP"""

    permission = Permission.VIEW

    def get(self, request, **kwargs):
        config = SiteConfig.load()
        if not config.allow_zip_downloads:
            raise Http404()
        self.obj.make_zip()
        path = self.obj.zip_path()
        return xsendfile_redirect(path)
