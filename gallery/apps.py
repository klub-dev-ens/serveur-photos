from django.apps import AppConfig


class GalleryConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "gallery"
    verbose_name = "Galeries photos"

    def ready(self) -> None:
        """Connects signals"""
        from . import signals  # noqa: F401
