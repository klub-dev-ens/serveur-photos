# Serveur photos

[![Python version][pyversion-shield]][pyversion-link]
[![Django version][django-shield]][django-link]
[![License][license-shield]][license-link]
[![Website][website-shield]]()

[![Pipeline][pipeline-shield]][pipeline-link]
[![Coverage][coverage-shield]][coverage-link]
[![Issues][issues-shield]][issues-link]
[![Merge Requests][mr-shield]][mr-link]

[pyversion-shield]: https://img.shields.io/badge/python-3.10-blue?logo=python&logoColor=white
[pyversion-link]: https://www.python.org/downloads/release/python-3107/

[django-shield]: https://img.shields.io/badge/django-4.1.7-blue?logo=django&logoColor=white
[django-link]: https://docs.djangoproject.com/en/4.1/releases/4.1.7/

[license-shield]: https://img.shields.io/gitlab/license/klub-dev-ens/serveur-photos?gitlab_url=https%3A%2F%2Fgit.eleves.ens.fr&color=green
[license-link]: ./LICENSE

[website-shield]: https://img.shields.io/website?down_color=red&down_message=Not%20yet%20deployed&up_color=brightgreen&url=https%3A%2F%2Fnot.yet.deployed

[pipeline-shield]: https://img.shields.io/gitlab/pipeline-status/klub-dev-ens/serveur-photos?branch=master&gitlab_url=https%3A%2F%2Fgit.eleves.ens.fr&label=tests
[pipeline-link]: https://git.eleves.ens.fr/klub-dev-ens/serveur-photos/-/pipelines

[coverage-shield]: https://img.shields.io/gitlab/pipeline-coverage/klub-dev-ens/serveur-photos?branch=master&gitlab_url=https%3A%2F%2Fgit.eleves.ens.fr
[coverage-link]: https://git.eleves.ens.fr/klub-dev-ens/serveur-photos/-/jobs

[issues-shield]: https://img.shields.io/gitlab/issues/open/klub-dev-ens/serveur-photos?gitlab_url=https%3A%2F%2Fgit.eleves.ens.fr
[issues-link]: https://git.eleves.ens.fr/klub-dev-ens/serveur-photos/-/issues

[mr-shield]: https://img.shields.io/gitlab/merge-requests/open/klub-dev-ens/serveur-photos?gitlab_url=https%3A%2F%2Fgit.eleves.ens.fr
[mr-link]: https://git.eleves.ens.fr/klub-dev-ens/serveur-photos/-/merge_requests

Ce répo contient le code source du serveur photo. Il est diffusé sous une
[license MIT](https://choosealicense.com/licenses/mit/).

- [Information administrateurs](#information-administrateurs)
- [Installation](#installation)
- [Information développeurs](#information-développeurs)
	- [Style de code](#style-de-code)
	- [Script pre-commit](#script-pre-commit)
	- [CI](#ci)
	- [SCSS](#scss)
	- [Dépendances incluses](#dépendances-incluses)
- [En production](#en-production)

## Information administrateurs

Le serveur photo organise les photos en dossiers et galeries :

- Les galeries (Gallery) contiennent les photos (et rien d'autre). Elles ont un
  système de permission spécifique (car Django ne gère pas les permissions par
  objet, uniquement par table)

	Chaque galerie a 4 types de permissions que l'on peut assigner à des
	groupes (clipper/cof/bds...) ou à des comptes individuels :

	- Voir (voir les photos)
	- Ajouter des photos. Les site converti toute nouvelle photo ajouté au format JPEG
	  afin d'avoir un affichage correct sur le plus d'appareils possible (discutable, maybe revert?)
	- Supprimer des photos - cela ne
		supprime pas vraiment les fichiers, cela les cache juste. Ils peuvent être
		restaurés ou supprimés définitivement par ceux qui ont la permission "gérer"
	- Gèrer - renommer, changer l'URL, le dossier parent, la vignette,
  	restaurer/supprimer définitivement les photos masquées.

	Additionnellement, il est possible de rendre une galerie publique (tout le
	monde peut la voir, même sans connexion), ou d'y créer un lien d'accès qui
	permet à ceux qui l'ont de la voir (par exemple pour donner accès aux photos
	de la Nuit aux extés inscrits).

- Les dossiers (Folder) contiennent d'autres dossiers et/ou galeries. Ils sont
  organisés en arbre (chaque dossier a un lien vers son parent, lien vide pour
  les fils de la racine). Les dossiers sont visibles si vide ou si l'une de
  leurs galeries est visible. Leur création et édition (tout comme la création
  de galeries) est gérée par des permissions Django standard.

La plupart des actions de gestion peuvent se faire directement depuis le site,
sans passer par la page d'admin (Créer/Éditer/Supprimer (si vide) des
galeries/dossiers, changer les vignettes, uploader/supprimer des photos).

Il faut passer par Django-admin (https://chemin_du_site/admin) pour créer des
comptes extés ou changer les groupes des utilisateurs (eg Burô du COF/BDS).

- Pour créer un compte exté, renseignez un email et mettez un mot de passe bidon
	puis dites à la personne concernée de se créer un mot de passe via le
	formulaire "Mot de passe oublié".
- Changer les groupes ou permissions d'un utilisateur se fait depuis son
  formulaire utilisateur. Il y a trois groupe hard-codé dans le site:
  - clipper (automatiquement ajouté à tout utilisateur se connectant 
    via clipper), a la permission "voir" par défaut sur toutes les nouvelles galeries
  - COF et BDS, déstinés au bureaux, a toutes les permissions sur les nouvelles galeries,
    a également toutes les permissions surs les dossiers, paramètres, utilisateurs, 
	groupes). Notez qu'il faut quand même donner à
    chaque utilisateur individuellement le `Statut équipe` pour qu'il puisse se
    connecter à Django-admin.

Afin d'éviter de faire fuiter de l'information sur l'organisation interne du
serveur, ce serveur n'utilise pas toujours les "bon" code de retour HTTP.
Spécifiquement, vous aurez le même code de retour si vous essayer d'accéder à
une galerie qui n'existe pas ou à une galerie dont vous n'avez pas la permission
d'accès (à savoir 302 vers la page de connexion si vous n'êtes pas connecté, 404
si vous l'êtes). De même pour les dossiers et les photos (les photos ne
renvoient jamais 302).

## Installation

Ce serveur nécessite une version de python >= 3.10 et de Django >= 4.1.

1. Il est conseillé d'utiliser un [environnement
   virtuel](https://docs.python.org/3/tutorial/venv.html), installable au choix
   depuis `apt` ou `pip` :

	```console
	sudo apt install python3-venv
	pip3 install venv
	```

2. Installez les dépendances systèmes :

	```console
	sudo apt install libldap2-dev libsasl2-dev python3-dev sassc
	```

3. Clonez ce répo et installez toutes les dépendances :

	```console
	git clone https://git.eleves.ens.fr/klub-dev-ens/serveur-photos.git &&
	cd serveur-photos &&
	python3 -m venv venv &&
	source venv/bin/activate &&
	make setup
	```

	Vous pouvez ensuite utilisez `make setup-dev` pour avoir les dépendances
	additionnelles (les checkeurs `mypy` et `flake8`, les formateurs `black` et
	`isort`, ainsi que le script qui les lance avant chaque commit).

4. (Optionnel) Installez le compilateur typescript `tsc` et le linteur
	`eslint` qui permettent la vérification de fichier JS.

	```bash
	sudo apt install nodejs npm
	npm install typescript eslint
	```

	Ces deux programmes sont nécessaires pour lancer le script precommit si vous
	modifiez des fichiers JS.

5. (Optionnel) initialisez la base de donnée avec des valeurs exemples

	```console
	make db
	```

	Cela créera des utilisateurs ("admin", "clipper", "cof", "bds", "exte") ayant
	tous le mot de passe `0000`, ainsi qu'une série de galeries et dossiers.

	Si vous voulez faire sans, vous pouvez créer un super utilisateur avec `make adduser`,
	ou vous connecter en clipper et vous donner les droits d'administration (`is_superuser` et `is_staff`) via la console (`make shell`)

6. Lancer le serveur :

	```console
	make serve
	```

	Le site devrait être accessible à [http://localhost:8000](http://localhost:8000). Vous pouvez interrompre le serveur avec `Ctrl+C` depuis le terminal.

## Information développeurs

### Style de code

Ce projet utilise certains formateurs et linteurs pour assurer un style de code
unifié :
- `black` (formateur python) et `isort` (trieur d'import python)
- `flake8` (linter python) pour vérifier les variables/import non utilisé
- `mypy` pour vérifier les types python statiquement. Pour qu'il soit vraiment utile il
  faut rajouter des annotations de types (pour les arguments/valeurs renvoyées
  de fonction, attributs de classe...)
	```python
	def foo(a: int, b: str) -> bool:
		...
	```
- `tsc` pour vérifier les types JS statiquement. Pour vérifier les types d'un
  fichier JS il faut rajouter un commentaire `// @ts-check` en début du fichier
  et spécifier les types de fonctions en commentaire avec la syntaxe JSDoc:
	```js
	/**
	 * @param {number} a
	 * @param {{x:number, y:number}} b
	 * @returns {boolean} (can be omitted on void functions)
	 */
	function foo(a, b) { return b.x >= b.y; }
	```
- `eslint` pour linter et formater les fichiers JS.

	Il n'est possible de vérifier que les fichiers JS (dans
	[gallery/static/js](gallery/static/js), et pas les bouts de `<script>` inclus
	dans les templates.

### Script pre-commit

Si vous installez avec `make setup-dev`, un script de pre-commit sera rajouter
pour les exécuter les checkeurs avant chaque commit. Les erreurs `mypy`, `tsc` et `flake8`
sont à corriger à la main tandis que `black` et `isort` renvoient des messages
d'erreur, mais reformatent directement les fichiers.

Notez que `tsc` n'est exécuté que s'il est installé, sinon cette vérification est ignorée.

S'il le faut vraiment, il est possible de sauter ces vérifications pour un commit avec
```console
git commit --no-verify
```

### CI

La CI gitlab lance tous les checkeurs (`black`, `isort`, `flake8`, `mypy`,
`tsc`), vérifie que le SCSS compile et lance les tests Django.

### SCSS

Les fichiers CSS sont générés à partir de fichiers SCSS (situés dans
[gallery/scss](gallery/scss/)). Pour les compiler faire `make css`. Cela nécessite
`sassc`, un compilateur simple. Vous pouvez utiliser n'importe quel autre
compilateur avec `make SASS=my_compiler css`.

### Dépendances incluses

Ce contient directement les fichiers issus de plusieurs dépendances externes :
- [fontawesome 6.1.1](https://fontawesome.com/) pour les jolies icônes. Pour le
  mettre à jour téléchargez les fichiers (de la version gratuite) et remplacez

	- le fichier de style
	[gallery/static/css/fontawesome.all.min.css](gallery/static/css/font-awesome-all.min.css)
	par le nouveau `all.min.css` - tout le contenu du dossier
	`gallery/static/webfonts/`

	Vérifiez au passage que les tags d'icône `<i class="..."><\i>` n'ont pas
	changé, sinon il faut également les changer dans les templates

	License: [Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT](https://fontawesome.com/license/free)

- [blueimp gallery 3.4.0](https://github.com/blueimp/Gallery) pour l'interface
  des galeries (affichage plein écran, diaporama...). Pour le mettre à jour exécutez

	```console
	npm install blueimp-gallery
	```

	License: [MIT](https://github.com/blueimp/Gallery/blob/master/LICENSE.txt)

	Puis copiez les dossiers `css`, `js`, et `img` de
	`node_modules/blueimp-gallery` dans [gallery/static](gallery/static/).

- [jQuery 3.6.1](https://jquery.com) nécessaire pour `justifiedGallery`. Pour la
  mettre à jour :

	```console
	npm install jquery
	cp node_modules/jquery/dist/jquery.min.js gallery/static/js/
	```

	License: [MIT](https://jquery.org/license/)

- [justifiedGallery 3.8.1](https://miromannino.github.io/Justified-Gallery/)
  pour l'affichage de galerie justifiée. Pour la mettre à jour

	```console
	npm install justifiedGallery
	cp node_modules/justifiedGallery/dist/js/jquery.justifiedGallery.min.js gallery/static/js/
	cp node_modules/justifiedGallery/dist/css/justifiedGallery.min.css gallery/static/css
	```

	License: [MIT](https://github.com/miromannino/Justified-Gallery/blob/master/LICENSE)

## En production

Le serveur a besoin d'être configuré pour HTTPS et d'être configuré pour livrer
directement les fichiers situés dans `/static/`.

En mode non Debug, Django ne sert pas les photos et thumbnails. Il faut
configurer le serveur pour qu'il serve les photos lorsqu'il reçoit une réponse
`X-Sendfile` de Django.

1. Copier le fichier [settings_example.py](serveurphotos/settings_example.py) à
	`serveurphotos/settings.py` et réparer les `FIXME`. Vous pouvez générer un
	secret Django avec

	```console
	python manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())'
	```

2. Installer les dépendances, générer le fichier CSS et faire les migrations
   ([Étapes 1-3](#installation)),

3. Faire un `make preprod` pour générer les fichiers statiques et vérifier les réglages
4. Créer un super utilisateur avec `makeadduser`, ou vous connecter en clipper
	et vous donner les droits d'administration (`is_superuser` et `is_staff`) via
	la console (`make shell`)
