"""Context processors used to add the
config to all templates"""

from typing import Any

from . import constants
from .models import SiteConfig


def config(request) -> dict[str, Any]:
    return {
        "settings": SiteConfig.load(),
        "constants": constants,
    }
