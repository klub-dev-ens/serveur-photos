"""This file contains constants accessible
- from python:    from config.constants import FOO
- from templates: {{ constants.FOO }}"""

from datetime import timedelta
from string import ascii_letters, digits
from typing import NamedTuple

# The website version number
WEBSITE_VERSION = "1.0.0"
WEBSITE_VERSION_DATE = "2022-10-16"

TITLE = "PhotENS"

WEBSITE_FULL_VERSION = "{} - {}".format(WEBSITE_VERSION, WEBSITE_VERSION_DATE)

# Update this to force reload of cached css
CSS_VERSION = "1.0"

# Footer info
KDE_EMAIL = "klub-dev [chez] ens.fr"
SOURCE_URL = "https://git.eleves.ens.fr/klub-dev-ens/serveur-photos/"
BUG_REPORT_URL = SOURCE_URL + "-/issues"

# Group names
GROUP_CLIPPER = "Clipper"
GROUP_COF = "COF"
GROUP_BDS = "BDS"
GROUP_TOUS = "Tous"


class EntityPermissions(NamedTuple):
    entity_name: str  # Group or user
    view: bool = False
    upload: bool = False
    delete: bool = False
    edit: bool = False


DEFAULT_GROUP_PERMISSIONS: list[EntityPermissions] = [
    EntityPermissions(GROUP_CLIPPER, view=True),
    EntityPermissions(GROUP_COF, view=True, upload=True, delete=True, edit=True),
    EntityPermissions(GROUP_BDS, view=True, upload=True, delete=True, edit=True),
]

# Separator in url path
FOLDER_PATH_SEPARATOR = "/"

# GalleryPassword parameter name in get REQUEST
KEY_QUERY_PARAM_NAME = "cle"

# Parameters used when generating new GalleryPasswords
KEY_LENGTH = 12
KEY_CHARS = ascii_letters + digits + "_-"
KEY_VALIDITY = timedelta(weeks=24)  # roughly 6 months

# Quality for image JPEG compression
# Ignored if image is already a JPEG
JPEG_IMAGE_QUALITY = 95
JPEG_THUMBNAIL_QUALITY = 75

IMAGE_DEFAULT_FILENAME = "unnamed"

SUPPORTED_FORMATS = [
    "jpg",
    "jpeg",
    "jpe",
    "png",
    "gif",
    "webp",
    "dds",
    "ppm",
    "eps",
    "fit",
    "fits",
    "pbm",
    "pcx",
    "pcc",
    "xbm",
    "tif",
    "tiff",
    "tga",
    "ps",
    "pnm",
    "pgm",
]
