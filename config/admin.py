from django.contrib import admin

from .models import SiteConfig


class SingletonModelAdmin(admin.ModelAdmin):
    """Prevents deletion or adding rows"""

    actions = None

    def has_add_permission(self, request, obj=None) -> bool:
        return False

    def has_delete_permission(self, request, obj=None) -> bool:
        return False


@admin.register(SiteConfig)
class SiteConfigAdmin(SingletonModelAdmin):
    pass
