from typing import TypeVar, cast

from django.core.cache import cache
from django.db import models

# Create a generic variable that can be 'Parent', or any subclass.
T = TypeVar("T", bound="SingletonModel")


class SingletonModel(models.Model):
    """Table de la BDD qui ne possède qu'un seul élément"""

    class Meta:
        abstract = True

    def save(self, *args, **kwargs) -> None:
        """save the unique element"""
        self.pk = 1  # set private key to one
        super(SingletonModel, self).save(*args, **kwargs)
        self.set_cache()

    def delete(self, *args, **kwargs) -> tuple[int, dict[str, int]]:
        """can't delete the unique element"""
        raise ValueError("Attempting to delete unique element")

    @classmethod
    def load(cls: type[T]) -> T:
        """load and return the unique element"""
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cast(T, cache.get(cls.__name__))

    def set_cache(self) -> None:
        """save in cache to limit db requests"""
        cache.set(self.__class__.__name__, self)


class SiteConfig(SingletonModel):
    """Réglages globaux du site,
    accessible:
    - dans les templates par {{ settings.XXX }}
    - dans le code par
        from config.models import SiteConfig

        config = SiteConfig.load()
        config.XXX"""

    class Meta:
        verbose_name = "paramètres"
        verbose_name_plural = "paramètres"

    nb_recent_galleries = models.PositiveIntegerField(
        verbose_name="Nombre de galeries récentes",
        help_text="Nombre de galleries récente à afficher sur la page d'acceuil",
        default=3,
    )

    allow_zip_downloads = models.BooleanField(
        verbose_name="Autoriser les téléchargements ZIP",
        default=True,
    )

    def __str__(self) -> str:
        return "Modifier les paramètres"
