# This makefile wraps around django commands
# for faster integrations

# See 'make help' for a list of useful targets

# ==================================================
# Constants
# ===================================================

PYTHON := python3
MANAGER := manage.py
PIP := $(PYTHON) -m pip
DB := db.sqlite3

DJANGO_PROJECT = serveurphotos

CSS_DIR = gallery/static/css/
SCSS_DIR = gallery/scss/
SCSS = style
SCSS_DEPS = variables mixins body header boxes style

# Without --checkJs, only checks files starting with @ts-check
TSC = tsc --allowJs --noEmit --target ES2015
JS_DIR = gallery/static/js
JS_FILES = $(JS_DIR)/*.js

SASS = sassc
SASSC = $(SASS) -t compressed

PRECOMMIT = pre-commit
MYPY = mypy

SETTINGS = $(DJANGO_PROJECT)/settings.py

# set to ON/OFF to toggle ANSI escape sequences
COLOR = ON

COVERAGE_OMIT = --omit=**/__init__.py,**/migrations/*,manage.py,$(DJANGO_PROJECT)/asgi.py,$(DJANGO_PROJECT)/wsgi.py

# Uncomment to show commands
# VERBOSE = TRUE

# padding for help on targets
# should be > than the longest target
HELP_PADDING = 15

# ==================================================
# Make code and variable setting
# ==================================================

ifeq ($(COLOR),ON)
	color_yellow = \033[93;1m
	color_orange = \033[33m
	color_red    = \033[31m
	color_green  = \033[32m
	color_blue   = \033[34;1m
	color_reset  = \033[0m
endif

define print
	@echo "$(color_yellow)$(1)$(color_reset)"
endef

CSS_FILES = $(addsuffix .css, $(addprefix $(CSS_DIR), $(SCSS)))
SCSS_DEPENDS = $(addsuffix .scss, $(addprefix $(SCSS_DIR), $(SCSS_DEPS)))
SCSS_TARGETS = $(addsuffix .scss, $(addprefix $(SCSS_DIR), $(SCSS)))

# =================================================
# Default target
# =================================================

default: serve
.PHONY: default

# =================================================
# Specific rules
# =================================================

$(SETTINGS):
	$(call print,Creating $@)
	ln -s "$(PWD)/serveurphotos/settings_example.py" $@

$(CSS_DIR)%.css: $(SCSS_DIR)%.scss $(SCSS_DEPENDS)
	$(call print,Compiling scss to $@)
	$(SASSC) "$<" "$@"

# =================================================
# Special Targets
# =================================================

# No display of executed commands.
$(VERBOSE).SILENT:

.PHONY: help
help: ## Show this help
	@echo "$(color_yellow)make:$(color_reset) list of useful targets:"
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  $(color_blue)%-$(HELP_PADDING)s$(color_reset) %s\n", $$1, $$2}'

.PHONY: serve
serve: $(SETTINGS) css ## Run the django server (blocking)
	$(call print,Running server (accessible from http://localhost:8000))
	$(PYTHON) $(MANAGER) runserver

.PHONY: do-migrate
do-migrate: $(SETTINGS)
	$(call print,Migrating database)
	$(PYTHON) $(MANAGER) makemigrations
	$(PYTHON) $(MANAGER) migrate

.PHONY: init-db
init-db: $(SETTINGS) ## Initialize DB permissions and groups (COF, BDS, ...)
	$(call print,Initializing DB permissions and groups)
	$(PYTHON) $(MANAGER) init_db

.PHONY: migrate
migrate: do-migrate init-db  ## Make and run migrations, init-db

.PHONY: db
db: $(SETTINGS) clean-db migrate ## Create a simple example database
	$(call print,Creating example database)
	$(PYTHON) $(MANAGER) make_example_db

.PHONY: clean-db
clean-db: ## Remove database
	$(call print,Removing database)
	rm -f $(DB)

.PHONY: clean-all
clean-all: ## Remove migrations and delete database
	$(call print,Removing migrations and database)
	find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "*/venv/*" -delete
	find . -path "*/migrations/*.pyc" -not -path "*/venv/*" -delete
	rm -f $(DB)
	rm -f $(CSS_FILES)

.PHONY: adduser
adduser: $(SETTINGS) ## Create a new superuser
	$(call print,Creating a new superuser)
	$(PYTHON) $(MANAGER) createsuperuser

.PHONY: shell
shell: $(SETTINGS) ## Run django's shell
	$(call print,Starting django's shell)
	$(PYTHON) $(MANAGER) shell

.PHONY: css
css: $(CSS_FILES) ## (re)build css files from scss

.PHONY: css-watch
css-watch: ## continuously rebuild css files (blocking)
	while [ 0 ]; \
	do \
		$(MAKE) --no-print-directory css; \
		sleep 1; \
	done

# =================================================
# Tests and checks
# =================================================

.PHONY: precommit
precommit: ## run precommit
	$(call print,Running precommit)
	$(PRECOMMIT) run

.PHONY: precommit-all
precommit-all: ## run precommit on all files
	$(call print,Running precommit on all files)
	$(PRECOMMIT) run --all-files

.PHONY: static
static: $(SETTINGS) css ## collect static files
	$(call print,Collecting static files)
	$(PYTHON) $(MANAGER) collectstatic

.PHONY: test
test: $(SETTINGS) ## Tests all the apps with django's tests
	$(call print,Running django tests)
	$(PYTHON) -Wa -m coverage run --source='.' $(MANAGER) test --noinput
	$(call print,Coverage report)
	rm -rf htmlcov
	coverage report $(COVERAGE_OMIT)
	coverage html $(COVERAGE_OMIT)

.PHONY: mypy
mypy: $(SETTINGS) ## Typecheck all python files
	$(call print,Typechecking python with mypy)
	$(MYPY) . --exclude /migrations/

.PHONY: tsc
tsc: ## Typecheck all JS files
	$(call print,Typechecking JS with tsc)
	$(TSC) $(JS_FILES)

.PHONY: format
format: ## Run formatters
	$(call print,Running black)
	black . --exclude "/(\.git|\.mypy_cache|\.venv|venv|migrations)/"
	$(call print,Running isort)
	isort . --gitignore --sg "*migrations*"

.PHONY: check-format
check-format: ## Run formatters, doesn't modify the files
	$(call print,Running black)
	black . --exclude "/(\.git|\.mypy_cache|\.venv|venv|migrations)/" --check
	$(call print,Running isort)
	isort . --gitignore --sg "*migrations*" --check

.PHONY: lint
lint: ## Run flake8 linter
	$(call print,Running flake8)
	flake8 . --exclude .git,.mypy_cache,.venv,venv,migrations

.PHONY: eslint
eslint: ## Run eslint linter
	$(call print,Running eslint)
	npx eslint --fix $(JS_DIR)

.PHONY: preprod
preprod: ## Prepare and check production
	$(PYTHON) $(MANAGER) check --deploy

genkey: ## Generate a new private key
	$(PYTHON) $(MANAGER) shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())'

# =================================================
# Installation
# =================================================

.PHONY: install
install:
	$(call print,Installing dependencies)
	$(PIP) install --upgrade pip
	$(PIP) install --upgrade -r requirements.txt

.PHONY: install-dev
install-dev:
	$(call print,Installing development dependencies)
	$(PIP) install --upgrade pip
	$(PIP) install --upgrade -r requirements-devel.txt

.PHONY: setup
setup: install $(SETTINGS) migrate css ## Install dependencies and make migrations

.PHONY: setup-dev
setup-dev: install-dev $(SETTINGS) migrate css ## Install development dependencies and make migrations
	$(call print,Setting up pre-commit)
	$(PRECOMMIT) install

settings: $(SETTINGS) ## Link settings_example to settings if non-existant
